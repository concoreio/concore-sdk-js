import axios from 'axios';
import Auth from './Datacore/Auth';
import Storage from './Datacore/Storage';

/**
 * Contains all Concore SDK classes and functions.
 */
class Concore {

  /**
   * Axios Instance
   * @type {Axios}
   */
  axios = null;

  /**
   * Concore SDK Version
   * @type {String}
   */
  version = '5.0.3';

  /**
   * Initialize Concore
   *
   * @param  {String} serverURL
   * @param  {String} appId
   * @param  {String} apiKey
   */
  init(serverURL, appId, apiKey) {
    if (typeof serverURL !== 'string') {
      throw new Error('serverURL must be a string.');
    }

    if (typeof appId !== 'string') {
      throw new Error('appId must be a string.');
    }

    if (typeof apiKey !== 'string') {
      throw new Error('apiKey must be a string.');
    }

    this.appId = appId;
    this.axios = axios.create({
      baseURL: serverURL,
      headers: {
        'Concore-APP-ID': appId,
        'Concore-APP-KEY': apiKey,
        'Concore-Server-URL': serverURL,
      }
    });

    this.axios.interceptors.request.use(config => {
      const newConfig = { ...config };
      const user = this.storage().getItem('currentUser');

      if (user && {}.hasOwnProperty.call(user, 'sessionToken')) {
        newConfig.headers['Concore-Session-Token'] = user.sessionToken;
      }

      return newConfig;
    }, error => Promise.reject(error));

    this.axios.interceptors.response.use(response => response, error => {
      const { response } = error;

      if (!response || !{}.hasOwnProperty.call(response, 'data')) {
        return Promise.reject(error);
      }

      const { errorCode } = response.data;

      if (errorCode && errorCode === 209) {
        Auth.logoutSync();
      }

      return Promise.reject(error);
    });
  }

  /**
   * Return Axios instance with methods
   *  - GET
   *  - POST
   *  - PUT
   *  - DELETE
   *  - PATCH
   *  - OPTIONS
   *
   * @return {axios}
   */
  request() {
    if (!this.axios) {
      throw new Error('You need initialize Concore before use the SDK.');
    }

    return this.axios;
  }

  status() {
    return this.request().get('/status')
      .then(response => response.data);
  }

  storage() {
    return new Storage(this.appId);
  }

}

/**
 * @ignore
 */
export default new Concore();
