import Flowcore from './Flowcore';

import Actions from './Actions';

Object.assign(Flowcore, {
  Actions
});

export default Flowcore;
