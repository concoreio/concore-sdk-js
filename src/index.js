import Concore from './Concore';
import Datacore from './Datacore';
import Flowcore from './Flowcore';
import Sighcore from './Sighcore';

Object.assign(Concore, {
  /**
   * @method Datacore
   * @static
   */
  Datacore,
  Flowcore,
  Sighcore
});

export default Concore;
