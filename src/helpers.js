/* eslint no-useless-escape: "off" */

/**
 * @ignore
 */
const helpers = {
  isNode() {
    return Object.prototype.toString.call(
      typeof process !== 'undefined' ? process : 0
    ) === '[object process]';
  },

  ucfirst(word) {
    return word.charAt(0).toUpperCase() + word.slice(1);
  },

  isEmpty(v) {
    if (helpers.toType(v) === 'object') {
      const len = Object.getOwnPropertyNames(v).length;
      return len === 0;
    }

    if (Array.isArray(v)) {
      return v.length === 0;
    }

    return v === null || v === undefined || v === '';
  },

  slugify(text) {
    const a = 'àáäâèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;';
    const b = 'aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh______';
    const p = new RegExp(a.split('').join('|'), 'g');

    return text.toString().toLowerCase()
        .replace(/\s+/g, '_')           // Replace spaces with _
        .replace(p, c =>
          b.charAt(a.indexOf(c)))     // Replace special chars
        .replace(/&/g, '_and_')         // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '_')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');             // Trim - from end of text
  },

  solveConfigId(config = {}, id = null) {
    if (helpers.toType(config) === 'string') {
      return {
        config: {},
        id: config
      };
    } else if (helpers.toType(config) === 'object') {
      const newConfig = { ...config };
      delete newConfig.id;
      return {
        config: newConfig,
        id: id || config.id
      };
    }

    return {
      config: {},
      id: null
    };
  },

  toType(obj) {
    return ({}).toString.call(obj).match(/\s([a-z|A-Z]+)/)[1].toLowerCase();
  }
};

/**
 * @ignore
 */
export default helpers;
