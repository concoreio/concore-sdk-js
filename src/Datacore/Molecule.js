import { toType } from '../helpers';
import Concore from '../Concore';
import ACL from './ACL';
import AtomFile from './AtomFile';
import AccessoryFactory from './AccessoryFactory';

/**
 * Private Attributes from Molecule
 * @type {WeakMap}
 */
const _attrs = new WeakMap();

/**
 * Creates a new Molecule.
 *
 * @example
 *
 * const paulo = new Molecule('Person', {
 *  name: 'Paulo Reis',
 *  email: 'paulovitin@datacore.com',
 *  birth: new Date(1986, 5, 23),
 *  gender: 'M',
 *  photo: uploadFile.data, // after file uploaded
 * });
 *
 * paulo.save()
 *  .then(pauloSaved => {
 *    // save with success
 *  })
 *  .catch(error => {
 *    // failed save
 *  });
 *
 * paulo.set('name', 'Paulo Vítor Reis').save()
 *  .then(pauloSaved => {
 *    // update with success
 *  })
 *  .catch(error => {
 *    // failed update
 *  });
 */
export default class Molecule extends AccessoryFactory({
  data: _attrs,
}) {

  /**
   * @param  {String} moleculoidName
   * @param  {Array}  atoms
   * @return {Molecule}
   */
  constructor(moleculoidName, atoms = {}) {
    super();
    if (toType(moleculoidName) !== 'string' || moleculoidName === '') {
      throw new Error('Moleculoid name must be a string and is required');
    }

    if (toType(atoms) !== 'object') {
      throw new Error('Atoms must be an object of name and value');
    }

    let id;
    const newAtoms = { ...atoms };
    if ({}.hasOwnProperty.call(atoms, 'id')) {
      id = atoms.id;
      delete newAtoms.id;
    }


    _attrs.set(this, {
      id,
      moleculoid: moleculoidName,
      atoms: newAtoms,
    });
  }

  /**
   * Return class name
   * @return {String}
   */
  get className() {
    return 'Molecule';
  }

  /**
   * Return a Molecule Representation
   * @return {Object}
  */
  get attributes() {
    const attrs = _attrs.get(this);
    const attributes = {
      ...attrs,
      atoms: { ...attrs.atoms },
    };

    Object.keys(attributes.atoms).forEach(key => {
      const { __op } = attributes.atoms[key] || {};
      if (__op === 'Delete') {
        attributes.atoms[key] = null;
      }
    });

    return attributes;
  }

  /**
   * Return Molecule ID
   * @return {String}
   */
  getId() {
    return this.attributes.id;
  }

  /**
   * Destroy this Molecule on the server if it was already persisted
   * @return {Promise}
   */
  destroy() {
    const attributesJSON = this._toSaveJSON();
    const attributes = this.attributes;
    const { moleculoid, id } = this.attributes;

    if (!attributes.id) {
      return Promise.reject(`The ${moleculoid} needs a ID to destroy.`);
    }

    return new Promise((resolve, reject) => {
      Concore.request()
        .delete(`/molecule/${moleculoid}/${id}`, attributesJSON)
        .then(() => {
          delete attributes.id;
          delete attributes.createdAt;
          delete attributes.updatedAt;
          _attrs.set(this, {
            ...attributes
          });

          resolve(this);
        })
        .catch(error => reject(error.response));
    });
  }

  /**
   * Return the Moleculoid name for this Molecule
   * @return {String}
   */
  getMoleculoidName() {
    return _attrs.get(this).moleculoid;
  }

  /**
   * Return the title value of this Molecule
   * @return {String}
   */
  getTitle() {
    const atoms = this.getAtoms();
    const key = Object.keys(atoms).shift();

    if (!atoms[key]) {
      return null;
    }

    const value = atoms[key];

    if (toType(value) !== 'object') {
      return value;
    }

    if ({}.hasOwnProperty.call(value, 'formatted_address')) {
      return value.formatted_address;
    }

    if ({}.hasOwnProperty.call(value, 'label')) {
      return value.label;
    }

    if ({}.hasOwnProperty.call(value, 'id')) {
      const keyRef = Object.keys(value).filter(k => k !== 'id').pop();
      return value[keyRef];
    }
  }

  /**
   * Set atom value by name to this Molecule
   * @param {String|Object} name
   * @param {Any} value
   * @return {Molecule}
   */
  set(name, value) {
    return this.setAtoms(name, value);
  }

  /**
   * Set atom value by name to this Molecule
   * @param {String|Object} name
   * @param {Any} value
   * @return {Molecule}
   */
  setAtoms(name, value) {
    if (name === 'ACL') {
      throw new TypeError('For set ACL use method setACL');
    }

    const obj = _attrs.get(this);
    let { atoms } = obj;

    if (toType(name) === 'object') {
      atoms = {
        ...atoms,
        ...name
      };
    } else if (toType(name) === 'string') {
      atoms = {
        ...atoms,
        [name]: value
      };
    }

    _attrs.set(this, {
      ...obj,
      atoms: { ...atoms }
    });

    return this;
  }

  /**
   * Save the Molecule to the server
   * @return {Promise}
   */
  save() {
    const attributesJSON = this._toSaveJSON();
    const attributes = this.attributes;
    const { moleculoid } = this.attributes;

    if (!attributes.atoms) {
      return Promise.reject('The Molecule required atoms to save.');
    }

    return new Promise((resolve, reject) => {
      Concore.request()
        .post(`/molecule/${moleculoid}`, attributesJSON)
        .then(response => {
          _attrs.set(this, {
            ...attributes,
            ...response.data
          });

          resolve(this);
        })
        .catch(error => reject(error));
    });
  }

  /**
   * Return a list of Atoms or a atom by metatom id
   * @param {String} name
   * @return {Array}
   */
  get(name) {
    return this.getAtoms(name);
  }

  /**
   * Return a list of Atoms or a atom by metatom id
   * @param {String} name
   * @return {Array}
   */
  getAtoms(name) {
    const atoms = _attrs.get(this).atoms;
    if (name) {
      let value = atoms[name];
      if (value && {}.hasOwnProperty.call(value, '__op') && value.__op === 'Delete') {
        value = null;
      }

      return value;
    }

    return atoms;
  }

  /**
   * Remove a Atom from Atoms list
   * @param  {Object|String} name
   * @return {Molecule}
   */
  removeAtoms(name) {
    const _protected = ['id', 'createdAt', 'updatedAt'];
    const obj = _attrs.get(this);
    const { atoms } = obj;

    if (_protected.includes(name)) {
      throw new Error(`You cannot remove ${_protected.join(', ')}`);
    }

    atoms[name] = {
      __op: 'Delete',
    };
    _attrs.set(this, {
      ...obj,
      atoms: { ...atoms }
    });

    return this;
  }

  /**
   * Returns a JSON version of the object
   * @return {Object}
   */
  toJSON() {
    let acl = this.getACL();
    let json = {};

    if (acl) {
      json = {
        ...this.attributes,
        ACL: { ...acl },
        atoms: { ...this.attributes.atoms },
      };
    } else {
      json = {
        ...this.attributes,
        ACL: {},
        atoms: { ...this.attributes.atoms },
      };
    }

    Object.keys(json.atoms).forEach(key => {
      const atom = json.atoms[key];
      if (atom instanceof AtomFile) {
        json.atoms[key] = { ...atom.toJSON() };
      }
    });

    return json;
  }

  /**
   * Try restore Molecule in the server
   * @static
   * @method restore
   * @param {String} moleculoid Moleculoid name
   * @param {String} moleculeId Molecule id to try restore
   * @return {Promise}
   */
  static restore(moleculoid, moleculeId) {
    return new Promise((resolve, reject) => {
      const path = `/molecule/${moleculoid}/${moleculeId}`;
      const url = `${path}/restore`;

      Concore.request()
        .patch(url)
        .then(() => Concore.request().get(path))
        .then(response => {
          const { data } = response;
          if (!data) {
            return resolve();
          }

          resolve(Molecule._inflate(moleculoid, data));
        })
        .catch(error => reject(error));
    });
  }

  /**
   * @ignore
   */
  unset(name) {
    const obj = _attrs.get(this);
    const { atoms } = obj;

    if ({}.hasOwnProperty.call(atoms, name)) {
      delete atoms[name];
    }

    _attrs.set(this, {
      ...obj,
      atoms: { ...atoms }
    });

    return this;
  }

  /**
   * @ignore
   */
  _toSaveJSON() {
    const attributes = _attrs.get(this);
    const json = {
      ...attributes,
      atoms: { ...attributes.atoms },
    };

    Object.keys(json.atoms).forEach(key => {
      const atom = json.atoms[key];
      if (atom instanceof AtomFile) {
        json.atoms[key] = atom.toJSON();
      }
    });

    return json;
  }

  /**
   * @ignore
   */
  static queryDocuments(moleculoidName, query = '', page = 0, limit = null) {
    return Molecule._query(
      `/molecule/${moleculoidName}/documents/search`,
      moleculoidName,
      query,
      page,
      limit
    );
  }

  /**
   * @ignore
   */
  static _query(url, moleculoidName, query = '', page = 0, limit = null) {
    if (!/^[a-zA-Z][_a-zA-Z0-9]*$/.test(moleculoidName)) {
      throw new Error('Moleculoid name is invalid!');
    }

    const queryString = `q=${query}&page=${page}&limit=${limit || ''}`;

    return new Promise((resolve, reject) => {
      Concore.request()
        .get(`${url}?${queryString}`)
        .then(response => {
          const { data } = response;

          if (data.results.length > 0) {
            data.results = data.results.map(
              item => Molecule._inflate(moleculoidName, item)
            );
          }

          return resolve(data);
        })
        .catch(error => reject(error.response));
    });
  }

  /**
   * @ignore
   */
  static _inflateAtomFiles(molecule) {
    const atoms = molecule.getAtoms();

    Object.keys(atoms).forEach(key => {
      const atom = atoms[key];
      if (typeof atom !== 'object' || atom === null) {
        return true;
      }

      if (atom._class !== 'AtomFile') {
        return true;
      }

      const instance = !Array.isArray(atom) ?
        this._inflateAtomFile(atom) : atom.map(item => this._inflateAtomFile(item));

      molecule.set(key, instance);
    });
  }

  /**
   * @ignore
   */
  static _inflateAtomFile(data) {
    const instance = new AtomFile();
    instance.data = data;

    return instance;
  }

  /**
   * @ignore
   */
  static _inflate(name, data) {
    const instance = new Molecule(name, data);
    _attrs.set(instance, {
      ...data,
      moleculoid: name,
    });

    if ({}.hasOwnProperty.call(data, 'ACL') && data.ACL) {
      const values = _attrs.get(instance);
      _attrs.set(instance, {
        ...values,
        ACL: new ACL(data.ACL),
      });
    }

    this._inflateAtomFiles(instance);

    return instance;
  }
}
