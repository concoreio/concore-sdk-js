import { toType } from '../helpers';
import Metatom from './Metatom';

/**
 * Manager a Collection of Metatoms to Moleculoid
 *
 * @ignore
*/
export default class MetatatomManager {

  /**
   *
   * @param  {Object} context
   * @param  {WeakMap} weakmap
   * @param  {string} key
   * @return {MetatomManager}
   */
  constructor(context, weakmap, key) {
    if (!context || toType(context) !== 'object') {
      throw new TypeError('Metatatom needs a context and must be an Object.');
    }

    if (!(weakmap instanceof WeakMap)) {
      throw new TypeError('Metatatom needs weakmap and must be an WeakMap instance.');
    }

    if (!key) {
      throw new TypeError('Metatatom needs a key to map the collection.');
    }

    if (!weakmap.get(context)) {
      throw new TypeError('WeakMap dont has a mapping to the context informed. Fix this.');
    }

    this.weakmap = weakmap;
    this.context = context;
    this.key = key;
    this.collection = [];
  }

  /**
   * Set the Collection to Moleculoid weakmap context
   * @param  {Object} collection
   */
  set collection(collection) {
    const _attributes = this.weakmap;
    const context = this.context;
    const key = this.key;

    const attrs = _attributes.get(context);
    attrs[key] = collection;
    _attributes.set(context, { ...attrs });
  }

  /**
   * Return the collection from the Moleculoid weakmap context
   * @return {Array}
   */
  get collection() {
    const _attributes = this.weakmap;
    const context = this.context;
    const key = this.key;

    const attrs = _attributes.get(context);
    return attrs[key];
  }

  /**
   * Add an array of Metatoms to Moleculoid
   * @param {Array} items
   * @return {MetatomManager}
   */
  add(items) {
    let list = items;
    if (!Array.isArray(list)) {
      list = [list];
    }

    list.forEach(item => this.addItem(item));
    return this;
  }

  /**
   * Add Metatom to Moleculoid
   * @param {Metatom} item
   */
  addItem(item) {
    if (!(item instanceof Metatom)) {
      throw new TypeError(`item must be an instance of ${Metatom.className}`);
    }

    if (this.collection.find(obj => obj.attributes.id === item.attributes.id)) {
      throw new Error(`${item.attributes.id} already exists`);
    }

    this.collection.push(item);
    return this;
  }

  /**
   * Get the Metatom for give ID or all metatoms
   * @param  {String} id
   * @return {Metatom|Array}
   */
  get(id) {
    if (!id) {
      return this.collection;
    }
    return this.collection.find(item => item.attributes.id === id);
  }

  /**
   * Remove an array of Metatoms from the Moleculod
   * @param  {Array} items
   * @return {MetatomManager}
   */
  remove(items) {
    let list = items;
    if (!Array.isArray(list)) {
      list = [items];
    }

    list.forEach(item => this.removeItem(item));
    return this;
  }

  /**
   * Remove Metatom from the Moleculod
   * @param  {Array} items
   * @return {MetatomManager}
   */
  removeItem(item) {
    if (item instanceof Metatom) {
      this.collection = this.collection.filter(obj => obj !== item);
      return this;
    }

    this.collection = this.collection.filter(obj => obj.attributes.id !== item);
    return this;
  }

  /**
   * Return the JSON from collection
   * @return {Object}
   */
  toJSON() {
    const data = this.collection.map(item => item.toJSON());

    return [...data];
  }
}
