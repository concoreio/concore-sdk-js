import BooleanType from './MetatomTypes/BooleanType';
import Choice from './MetatomTypes/Choice';
import Currency from './MetatomTypes/Currency';
import DateTime from './MetatomTypes/DateTime';
import Duration from './MetatomTypes/Duration';
import Email from './MetatomTypes/Email';
import File from './MetatomTypes/File';
import Image from './MetatomTypes/Image';
import Document from './MetatomTypes/Document';
import Json from './MetatomTypes/Json';
// import List from './MetatomTypes/List';
import Location from './MetatomTypes/Location';
import NumberType from './MetatomTypes/NumberType';
import Password from './MetatomTypes/Password';
import Reference from './MetatomTypes/Reference';
import RouteLocation from './MetatomTypes/RouteLocation';
import Text from './MetatomTypes/Text';
import Url from './MetatomTypes/Url';


/**
 * Metatom Types
 */
export default class MetatomType {
  /**
   * MetatomType.BooleanType
   * @type {BooleanType}
   */
  static BooleanType = BooleanType;
  /**
   * MetatomType.Choice
   * @type {Choice}
   */
  static Choice = Choice;
  /**
   * MetatomType.Currency
   * @type {Currency}
   */
  static Currency = Currency;
  /**
   * MetatomType.DateTime
   * @type {DateTime}
   */
  static DateTime = DateTime;
  /**
   * MetatomType.Document
   * @type {Document}
   */
  static Document = Document;
  /**
   * MetatomType.Duration
   * @type {Duration}
   */
  static Duration = Duration;
  /**
   * MetatomType.Email
   * @type {Email}
   */
  static Email = Email;
  /**
   * MetatomType.File
   * @type {File}
   */
  static File = File;
  /**
   * MetatomType.Image
   * @type {Image}
   */
  static Image = Image;
  /**
   * MetatomType.Json
   * @type {Json}
   */
  static Json = Json;
  // /**
  //  * MetatomType.List
  //  * @type {List}
  //  */
  // static List = List;
  /**
   * MetatomType.Location
   * @type {Location}
   */
  static Location = Location;
  /**
   * MetatomType.NumberType
   * @type {NumberType}
   */
  static NumberType = NumberType;
  /**
   * MetatomType.Password
   * @type {Password}
   */
  static Password = Password;
  /**
   * MetatomType.Reference
   * @type {Reference}
   */
  static Reference = Reference;
   /**
   * MetatomType.RouteLocation
   * @type {RouteLocation}
   */
  static RouteLocation = RouteLocation;
  /**
   * MetatomType.Text
   * @type {Text}
   */
  static Text = Text;
  /**
   * MetatomType.Url
   * @type {Url}
   */
  static Url = Url;
}
