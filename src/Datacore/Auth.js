import Concore from '../Concore';
import Molecule from './Molecule';

/**
 * Private Attributes from Auth
 * @type {WeakMap}
 */
const _attrs = new WeakMap();

/**
 * Concore.Auth
 */
export default class Auth {

  /**
   * Return the Storage client
   * @return {Storage}
   */
  static getStorage() {
    return Concore.storage();
  }

  /**
   * Return current user
   * @return {Object}
   */
  static getUser() {
    const attributes = _attrs.get(this) || {};
    let user = attributes.user;

    if (!{}.hasOwnProperty.call(attributes, 'user') || !user) {
      const userJSON = this.getStorage().getItem('currentUser');

      if (userJSON) {
        user = Molecule._inflate('User', userJSON);
        _attrs.set(this, {
          ...attributes,
          sessionToken: userJSON.sessionToken,
          user,
        });
      }
    }

    return user;
  }

  static get id() {
    return this.getUser() ? this.getUser().id : null;
  }

  static get sessionToken() {
    let attributes = _attrs.get(this);

    if (!attributes) {
      this.getUser();
      attributes = _attrs.get(this);
    }

    return attributes ? attributes.sessionToken : null;
  }

  static become(sessionToken) {
    return new Promise((resolve, reject) => {
      Concore.request().get('/auth', {
        headers: {
          'concore-session-token': sessionToken,
        }
      }).then(response => {
        const user = response.data;

        _attrs.set(this, {
          sessionToken: user.sessionToken,
          user: Molecule._inflate('User', user),
        });
        this.getStorage().setItem('currentUser', user);
        return resolve(user);
      })
      .catch(reject);
    });
  }

  /**
   * Check if user is logged
   * @return {Boolean}
   */
  static isLogged() {
    return !!this.getStorage().getItem('currentUser');
  }

  /**
   * Try to login on Concore
   *
   * @param  {String} username
   * @param  {String} password
   * @return {Promise}
   */
  static login(username, password) {
    if (Auth.isLogged()) {
      const user = Auth.getUser();

      try {
        if (username === user.getAtoms('username')) {
          return Promise.resolve(user);
        }
      } catch (error) { /* ignore */ }

      Auth.logoutSync();
    }

    if (typeof username !== 'string') {
      return Promise.reject('Username must be a string.');
    } else if (typeof password !== 'string') {
      return Promise.reject('Password must be a string.');
    }

    return new Promise((resolve, reject) => {
      Concore.request()
        .post('/auth', {
          username,
          password,
        })
        .then(response => {
          const user = response.data;
          if (!{}.hasOwnProperty.call(user, 'sessionToken') || !user.sessionToken) {
            return reject(user);
          }

          _attrs.set(this, {
            sessionToken: user.sessionToken,
            user: Molecule._inflate('User', user),
          });
          this.getStorage().setItem('currentUser', user);
          return resolve(Auth.getUser());
        });
    });
  }

  /**
   * Try to logout on Concore
   * @return {Promise}
   */
  static logout() {
    if (!Auth.isLogged()) {
      return Promise.reject('User is not logged in.');
    }

    return new Promise((resolve, reject) => {
      Concore.request()
        .delete('/auth')
        .then(() => {
          this.getStorage().removeItem('currentUser');
          return resolve();
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  /**
   * Logout user synchronously
   */
  static logoutSync() {
    _attrs.set(this, {});
    this.getStorage().removeItem('currentUser');
  }
}
