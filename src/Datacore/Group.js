import { toType } from '../helpers';
import Concore from '../Concore';
import Moleculoid from './Moleculoid';

/**
 * Private Attributes from Group
 * @type {WeakMap}
 */
const _attrs = new WeakMap();

export default class Group {

  static withMoleculoids = false;

  constructor(name, color = '#FFFFFF', moleculoids = []) {
    if (!name) {
      throw new Error('Group name is required.');
    }

    _attrs.set(this, {
      color,
      name,
    });

    this.setMoleculoids(moleculoids);
  }

  get attributes() {
    return _attrs.get(this) || {};
  }

  /**
   * Return class name
   * @return {String}
   */
  get className() {
    return 'Group';
  }

  get id() {
    return this.attributes.id;
  }

  addMoleculoids(moleculoids) {
    const list = Array.isArray(moleculoids) ? moleculoids : [moleculoids];

    list.forEach(item => {
      if (!toType(item) === 'string' && !(item instanceof Moleculoid)) {
        throw new TypeError('Moleculoids must be a string or instance of Moleculoid.');
      }
    });

    const current = this.attributes.moleculoids || [];

    list.forEach(item => {
      let moleculoidName = item;
      let objItem = {
        name: item,
      };

      if (item instanceof Moleculoid) {
        moleculoidName = item.getName();
        objItem = item;
      }

      if (!current.find(moleculoid => moleculoid.name === moleculoidName)) {
        current.push(objItem);
      }
    });

    _attrs.set(this, {
      ...this.attributes,
      moleculoids: current,
    });

    return this;
  }

  destroy() {
    if (!this.attributes.id) {
      throw new Error('Cannot delete group without an id');
    }

    const { id } = this.attributes;

    return new Promise((resolve, reject) => {
      Concore.request()
        .delete(`/group/${id}`)
        .then(() => {
          _attrs.set(this, {});
          resolve(this);
        })
        .catch(error => reject(error.response));
    });
  }

  getColor() {
    return this.attributes.color;
  }

  getMoleculoids(name) {
    if (!name) {
      return this.attributes.moleculoids;
    }

    return this.attributes.moleculoids.find(item => item.getName() === name);
  }

  getName() {
    return this.attributes.name;
  }

  removeMoleculoids(moleculoids) {
    const list = Array.isArray(moleculoids) ? moleculoids : [moleculoids];

    list.forEach(item => {
      if (!toType(item) === 'string' && !(item instanceof Moleculoid)) {
        throw new TypeError('Moleculoids must be a string or instance of Moleculoid.');
      }
    });

    let current = this.attributes.moleculoids || [];

    current = current.filter(moleculoid => !list.find(item => {
      let moleculoidName = item;
      if (item instanceof Moleculoid) {
        moleculoidName = item.getName();
      }

      return (moleculoid.name || moleculoid.getName()) === moleculoidName;
    }));

    _attrs.set(this, {
      ...this.attributes,
      moleculoids: current,
    });

    return this;
  }

  save() {
    const dataToSave = this.attributes;
    const { moleculoids } = this.attributes;

    dataToSave.moleculoids = moleculoids.map(item => {
      if (item instanceof Moleculoid) {
        return {
          id: item.id,
          name: item.getName(),
        };
      }

      return item;
    });

    return new Promise((resolve, reject) => {
      Concore.request().post('/group', dataToSave)
        .then(response => {
          const { data } = response;
          data.moleculoids = data.moleculoids.map(moleculoid =>
            Moleculoid._inflateMoleculoid(moleculoid),
          );

          _attrs.set(this, data);
          resolve(this);
        })
        .catch(error => reject(error.response));
    });
  }

  setColor(color) {
    if (toType(color) !== 'string') {
      throw new TypeError('Color must be a string.');
    }

    _attrs.set(this, {
      ...this.attributes,
      color,
    });

    return this;
  }

  setMoleculoids(moleculoids = []) {
    _attrs.set(this, {
      ...this.attributes,
      moleculoids: [],
    });

    return this.addMoleculoids(moleculoids);
  }

  setName(name) {
    if (toType(name) !== 'string') {
      throw new TypeError('Name must be a string.');
    }

    _attrs.set(this, {
      ...this.attributes,
      name,
    });

    return this;
  }

  static moleculoids() {
    this.withMoleculoids = true;

    return this;
  }

  static get(group) {
    return new Promise((resolve, reject) => {
      let url = '/group';

      if (group) {
        url += `/${group}`;
      }

      if (this.withMoleculoids) {
        url += '?withMoleculoid=true';
      }

      Concore.request()
        .get(url)
        .then(response => {
          const { data } = response;
          const { results } = data;

          if (results) {
            return resolve(results.map(groupData =>
              this._inflateGroup(groupData)
            ));
          }

          return resolve(this._inflateGroup(data));
        })
        .catch(error => reject(error));
    });
  }

  static _inflateGroup(group) {
    const groupInstance = new Group(group.name);
    const newGroup = { ...group };

    if (group.moleculoids) {
      newGroup.moleculoids = group.moleculoids.map(moleculoid =>
        Moleculoid._inflateMoleculoid(moleculoid),
      );
    }

    _attrs.set(groupInstance, newGroup);

    return groupInstance;
  }
}
