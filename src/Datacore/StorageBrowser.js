export default class StorageController {

  getItem(key) {
    return localStorage.getItem(key);
  }

  setItem(key, value) {
    try {
      localStorage.setItem(key, value);
    } catch (e) {
      // Quota exceeded, possibly due to Safari Private Browsing mode
    }
  }

  removeItem(key) {
    localStorage.removeItem(key);
  }

  clear() {
    localStorage.clear();
  }
}
