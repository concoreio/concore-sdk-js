import { isNode } from '../helpers';
import StorageBrowser from './StorageBrowser';
import StorageNode from './StorageNode';

/**
 * Concore Storage
 */
export default class Storage {

  /**
   * Create a instance of Storage
   * @param  {String} appId
   * @return {Storage}
   */
  constructor(appId) {
    if (!appId) {
      throw new Error('AppId is required.');
    }

    this.appId = appId;
    this.storage = isNode() ? new StorageNode() : new StorageBrowser();
  }

  /**
   * Return the value of the given key
   * @param  {String} key
   * @return {String}
   */
  getItem(key) {
    const value = this.storage.getItem(this._key(key));

    if (value === null) {
      return value;
    }

    try {
      return JSON.parse(value);
    } catch (e) {
      throw new TypeError('JSON invalid!');
    }
  }

  /**
   * Set the value of the given key
   * @param {String} key
   * @param {Any}    value
   */
  setItem(key, value) {
    this.storage.setItem(this._key(key), JSON.stringify(value));
  }

  /**
   * Remove the key of the storage
   * @param  {String} key
   */
  removeItem(key) {
    this.storage.removeItem(this._key(key));
  }

  /**
   * Clear the storage
   */
  clear() {
    this.storage.clear();
  }

  /**
   * Return the prefixed key for appId
   * @param  {String} key
   * @return {String}
   */
  _key(key) {
    return `concore.${this.appId}.${key}`;
  }
}
