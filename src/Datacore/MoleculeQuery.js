import { toType } from '../helpers';
import Concore from '../Concore';
import Molecule from './Molecule';

/**
 * Datacore.MoleculeQuery
 *
 * @module Datacore
 * @class Datacore.MoleculeQuery
 * @constructor
 * @param  {String} className
 * @return {Query}
 */
export default class MoleculeQuery {

  /**
   * Page number of results
   * @type {Number}
   */
  pageNumber = 0;

  /**
   * Limit size of results
   * @type {Number}
   */
  limitNumber = null;

  /**
   * Object Body Query
   * @type {Object}
   */
  body = {
    query: {
      bool: {
        must: [],
        must_not: [],
        filter: [],
      },
    },
    sort: [],
  };

  /**
   * Create an instance of Molecule Query
   * @param  {String} className Moleculoid Name
   */
  constructor(className) {
    this.className = className;
  }

  _contains(key, value) {
    this.body.query.bool.must.push({
      match: {
        [key]: {
          query: value,
        },
      },
    });

    return this;
  }

  _isEmail(s) {
    const regexp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regexp.test(s);
  }

  _isUrl(s) {
    /* eslint no-useless-escape: "off" */
    const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regexp.test(s);
  }

  _order(key, order, add = false) {
    if (!add) {
      this.body.sort = [];
    }

    this.body.sort.push({
      [key]: { order }
    });

    return this;
  }

  _term(key, condition, value) {
    let query = {};
    let typeMatch = 'match';
    let queryKey = 'filter';

    if (condition !== '=') {
      queryKey = 'must_not';
    }

    if (condition === '=') {
      queryKey = 'must';
    }

    if (condition === '=' && (this._isUrl(value) || this._isEmail(value))) {
      typeMatch = 'term';
    }

    if (toType(value) === 'object' && value.value) {
      query = {
        match: {
          [`${key}.value`]: value.value,
        },
      };
    } else if (toType(value) === 'array' && toType(value[0]) === 'object') {
      query = this._nested(key, value);
    } else {
      query = {
        [typeMatch]: {
          [key]: value,
        },
      };
    }

    this.body.query.bool[queryKey].push(query);

    return this;
  }

  _range(key, condition, value) {
    const range = {
      range: {
        [key]: {
          [condition]: value,
        },
      },
    };

    this.body.query.bool.filter.push(range);

    return this;
  }

  _existsQuery(key, exists = true) {
    const exist = {
      exists: {
        field: key,
      },
    };
    if (exists) {
      this.body.query.bool.filter.push(exist);
    } else {
      this.body.query.bool.must_not.push(exist);
    }

    return this;
  }

  _nested(key, value) {
    const filter = [];
    let subKey = 'value';

    if ({}.hasOwnProperty.call(value[0], 'id')) {
      subKey = 'id';
    }

    if (value.length > 1) {
      const values = value.map(val => val[subKey]);
      filter.push({
        terms: {
          [`${key}.${subKey}`]: values,
        },
      });
    } else {
      filter.push({
        match: {
          [`${key}.${subKey}`]: value[0][subKey],
        },
      });
    }

    return {
      nested: {
        path: key,
        query: {
          bool: {
            filter,
          },
        },
      },
    };
  }

  /**
   * Perform a query by the given keyword for all atoms
   * @param  {String} query
   * @param  {String} metatomId
   * @param  {Number|String} boost
   * @return {MoleculeQuery}
   */
  search(query, heavyColumn, boost = 2) {
    const fields = ['_all'];

    if (heavyColumn) {
      if (toType(heavyColumn) !== 'string') {
        throw new TypeError('The heavy column must be string');
      }

      fields.push(`${heavyColumn}^${boost}`);
      this.body.sort.unshift('_score');
    }

    this.body.query.bool.must.push({
      multi_match: {
        fields,
        query,
      },
    });

    return this;
  }

  /**
   * Add ascending sort to sort rule
   * @param {String} key metatom name
   * @return {MoleculeQuery}
   */
  addAscending(key) {
    return this._order(key, 'asc', true);
  }

  /**
   * Add descending sort to sort rule
   * @param {String} key metatom name
   * @return {MoleculeQuery}
   */
  addDescending(key) {
    return this._order(key, 'desc', true);
  }

  /**
   * Set ascending sort to sort rule
   * @param {String} key metatom name
   * @return {MoleculeQuery}
   */
  ascending(key) {
    return this._order(key, 'asc');
  }

  /**
   * Set descending sort to sort rule
   * @param {String} key metatom name
   * @return {MoleculeQuery}
   */
  descending(key) {
    return this._order(key, 'desc');
  }

  /**
   * Adds a constraint to the query that requires a particular key's value to
   * be equal to the provided value.
   * @param {String} key The key to check.
   * @param value The value that the Parse.Object must contain.
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  equalTo(key, value) {
    return this._term(key, '=', value);
  }

  /**
   * Adds a constraint for finding objects that contain the given key.
   * @method exists
   * @param {String} key The key that should exist.
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  exists(key) {
    return this._existsQuery(key);
  }

  /**
   * Adds a constraint for finding objects that do not contain a given key.
   * @method doesNotExist
   * @param {String} key The key that should not exist
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  doesNotExists(key) {
    return this._existsQuery(key, false);
  }

  /**
   * Adds a constraint to the query that requires a particular key's value to
   * be not equal to the provided value.
   * @method notEqualTo
   * @param {String} key The key to check.
   * @param value The value that must not be equalled.
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  notEqualTo(key, value) {
    return this._term(key, '!=', value);
  }

  /**
   * Adds a constraint to the query that requires a particular key's value to
   * be contained in the provided list of values.
   * @param {String} key The key to check.
   * @param {Array} values The values that will match.
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  contains(key, value) {
    return this._contains(key, value);
  }

  /**
   * Adds a constraint to the query that requires a particular key's value to
   * be greater than the provided value.
   * @param {String} key The key to check.
   * @param value The value that provides an lower bound.
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  greaterThan(key, value) {
    return this._range(key, 'gt', value);
  }

   /**
   * Adds a constraint to the query that requires a particular key's value to
   * be greater than or equal to the provided value.
   * @param {String} key The key to check.
   * @param value The value that provides an lower bound.
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  greaterThanOrEqualTo(key, value) {
    return this._range(key, 'gte', value);
  }

  /**
   * Adds a constraint to the query that requires a particular key's value to
   * be less than the provided value.
   * @param {String} key The key to check.
   * @param value The value that provides an upper bound.
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  lessThan(key, value) {
    return this._range(key, 'lt', value);
  }

  /**
   * Adds a constraint to the query that requires a particular key's value to
   * be less than or equal to the provided value.
   * @param {String} key The key to check.
   * @param value The value that provides an upper bound.
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  lessThanOrEqualTo(key, value) {
    return this._range(key, 'lte', value);
  }

  /**
   * Restricts the atoms of the returned Molecule to include only the
   * provided keys.  If this is called multiple times, then all of the keys
   * specified in each of the calls will be included.
   * @param {Array} atomNames The names of the atoms to include.
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  atoms(...atomNames) {
    let atoms = atomNames;

    if (Array.isArray(atomNames[0])) {
      atoms = atomNames[0];
    }

    this.body._source = atoms;

    return this;
  }

   /**
   * Sets the limit of the number of results to return.
   * @param {Number} n the number of results to limit to.
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  limit(limit = null) {
    this.limitNumber = limit;
    return this;
  }

   /**
   * Sets the limit of the number of results to return.
   * @param {Number} n the number of results to limit to.
   * @return {MoleculeQuery} Returns the query, so you can chain this call.
   */
  page(page = 0) {
    this.pageNumber = page;
    return this;
  }

  _url(path) {
    return `/molecule/${this.className}${path}`;
  }

  /**
   * Get Object by id
   *
   * @method get
   * @param  {String} id
   * @return {[type]}
   */
  get(id) {
    if (!id) {
      throw new Error('ID is required!');
    }

    return Concore.request()
      .get(this._url(`/${id}`))
      .then(response => Molecule._inflate(this.className, response.data));
  }

  /**
   * Find results based on informed filters
   *
   * @method find
   * @param  {Object} filters
   * @return Promise
   */
  find() {
    return this._request({
      elastic: this.body,
      limit: this.limitNumber,
      page: this.pageNumber,
    });
  }

  /**
   * Find First result based on informed filters
   *
   * @method first
   * @param  {Object} filters
   * @return Promise
   */
  first() {
    return this._request({
      elastic: this.body,
      limit: 1,
      page: this.pageNumber,
    })
    .then(response => response.results[0]);
  }

  _request(params) {
    return Concore.request()
      .get(this._url('/search'), {
        params,
      })
      .then(response => {
        const { data } = response;

        if (data.results.length > 0) {
          data.results = data.results.map(
            item => Molecule._inflate(this.className, item)
          );
        }

        return data;
      });
  }

  toJSON() {
    return { ...this.body };
  }

}
