/* eslint no-bitwise: "off" */
/* eslint no-useless-escape: "off" */
import Concore from '../Concore';

/**
 * @ignore
 */
const dataUriRegexp =
  /^data:([a-zA-Z]*\/[a-zA-Z+.-]*);(charset=[a-zA-Z0-9\-\/\s]*,)?base64,/;

/**
 * @ignore
 */
function b64Digit(number) {
  if (number < 26) {
    return String.fromCharCode(65 + number);
  }
  if (number < 52) {
    return String.fromCharCode(97 + (number - 26));
  }
  if (number < 62) {
    return String.fromCharCode(48 + (number - 52));
  }
  if (number === 62) {
    return '+';
  }
  if (number === 63) {
    return '/';
  }
  throw new TypeError(`Tried to encode large digit ${number} in base64.`);
}

/**
 * AtomFile is a local representation of a file that is saved to the Datacore
 *
 * @example
 *
 * const fileUploadField = document.querySelector('#upload');
 * const file = fileUploadField.files[0];
 * const name = 'My File';
 *
 * const uploadFile = new AtomFile(name, file);
 * uploadFile.save()
 *  .then(file => {
 *     //upload succcess
 *  })
 *  .catch(e => {
 *    // upload fail
 *  })
 */
export default class AtomFile {

  /**
   * Upload file object
   * @type {Object}
   */
  data = {};

  /**
   * File name
   * @type {String}
   */
  name = null;

  /**
   * @ignore
  */
  previousSave = false;

  /**
   * @ignore
  */
  source = {};

  /**
   * @param  {String} name
   * @param  {File|Array|Object} data
   * @param  {String} mimeType
   * @return {AtomFile}
   */
  constructor(name, data, mimeType = '') {
    this.name = name;

    if (data !== undefined) {
      if (Array.isArray(data)) {
        this.source = {
          name,
          format: 'base64',
          base64: AtomFile.encodeBase64(data),
          type: mimeType
        };
      } else if (typeof File !== 'undefined' && data instanceof File) {
        this.source = {
          name,
          format: 'file',
          file: data,
          type: mimeType
        };
      } else if (data && typeof data.base64 === 'string') {
        const base64 = data.base64;
        const commaIndex = base64.indexOf(',');

        if (commaIndex !== -1) {
          const matches = dataUriRegexp.exec(base64.slice(0, commaIndex + 1));
          this.source = {
            name,
            format: 'base64',
            base64: base64.slice(commaIndex + 1),
            type: matches[1]
          };
        } else {
          this.source = {
            name,
            format: 'base64',
            base64,
            type: mimeType
          };
        }
      } else {
        throw new TypeError('Cannot create a Atom.File with that data.');
      }
    }
  }

  /**
   * Return the mimeType from image
   * @return {String}
   */
  getMimeType() {
    return this.data && this.data.mimeType;
  }

   /**
   * Return the name from image
   * @return {String}
   */
  getName() {
    return this.data && this.data.name;
  }

  /**
   * Return the server url from image
   * @return {String}
   */
  getUrl() {
    return this.data && this.data.url;
  }

  /**
   * Remove file on the Datacore Server
   * @return {Promise}
   */
  destroy() {
    if (!this.data.url) {
      throw new TypeError('File not exists.');
    }

    return Concore.request()
      .delete(this.data.url)
      .then(() => {
        this.data = {};
      });
  }

  /**
   * Persist the file on the Datacore Server
   * @return {Promise}
   */
  save() {
    if (!this.previousSave) {
      if (this.source.format === 'file') {
        const { name, file, type } = this.source;
        const data = new FormData();
        data.append('file', file);

        this.previousSave = Concore.request()
          .post(`/files/${name}`, data, {
            headers: { 'Content-Type': type || (file ? file.type : null) }
          });
      } else {
        const { name, base64, type } = this.source;
        const data = {
          base64
        };

        if (type) {
          data.contentType = type;
        }

        this.previousSave = Concore.request()
          .post(`/files/${name}`, data);
      }
    }

    this.previousSave.then(res => {
      this.data = res.data;
    }).catch(() => {
      this.previousSave = null;
    });

    return this.previousSave;
  }

  toJSON() {
    return {
      ...this.data
    };
  }

  /**
   * Get atom file from the server
   * @static
   * @method get
   * @param {String} filename
   * @return {Promise}
   */
  static get(name) {
    if (!name) {
      throw new Error('name is required');
    }

    return Concore.request()
      .get(`/files/${name}`)
      .then(response => {
        const { data } = response;
        const file = new AtomFile();
        file.data = data;
        file.name = data.name;
        return file;
      });
  }

  /**
   * Find all atom files from the server
   * @static
   * @method find
   * @param {String} options Limit and pagination options
   * @return {Promise}
   */
  static find({ limit, page = 0 }) {
    return Concore.request()
      .get('/files', {
        params: {
          limit,
          page,
        },
      })
      .then(response => {
        const { data } = response;

        if (data.metadata.resultset.count <= 0) {
          return data;
        }
        const newResponse = {
          ...data,
        };

        newResponse.results = data.results.map(item => {
          const file = new AtomFile();
          file.data = item;
          file.name = item.name;
          return file;
        });

        return newResponse;
      });
  }

  /**
  * @ignore
  */
  static encodeBase64(bytes) {
    const chunks = [];
    chunks.length = Math.ceil(bytes.length / 3);

    for (let i = 0; i < chunks.length; i += 1) {
      const b1 = bytes[i * 3];
      const b2 = bytes[(i * 3) + 1] || 0;
      const b3 = bytes[(i * 3) + 2] || 0;

      const has2 = ((i * 3) + 1) < bytes.length;
      const has3 = ((i * 3) + 2) < bytes.length;

      chunks[i] = [
        b64Digit((b1 >> 2) & 0x3F),
        b64Digit(((b1 << 4) & 0x30) | ((b2 >> 4) & 0x0F)),
        has2 ? b64Digit(((b2 << 2) & 0x3C) | ((b3 >> 6) & 0x03)) : '=',
        has3 ? b64Digit(b3 & 0x3F) : '='
      ].join('');
    }

    return chunks.join('');
  }
}
