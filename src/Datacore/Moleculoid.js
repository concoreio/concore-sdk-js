import { toType } from '../helpers';
import Concore from '../Concore';
import ACL from './ACL';
import AccessoryFactory from './AccessoryFactory';
import Group from './Group';
import MetatomManager from './MetatomManager';
import MetaInflator from './MetatomTypeInflator';
import MetatomState from './MetatomState';
import Reference from './MetatomTypes/Reference';

/**
 * Private Attributes from Moleculoid
 * @private
 * @property {Object} attributes
 */
const _attrs = new WeakMap();
/**
 * Private MetatomManager from Moleculoid
 * @private
 * @property {MetatomManager} metatomManager
 */
const _metatomManager = new WeakMap();
/**
 * Creates a new Molecule schema.
 *
 * @module Datacore
 * @class Moleculoid
 * @extends AccessoryFactory
 * @constructor
 * @param  {String} name
 * @param  {Array}  metatoms
 * @param  {String} label
 * @return {Moleculoid}
 */
export default class Moleculoid extends AccessoryFactory({
  data: _attrs,
}) {

  constructor(name, metatoms = [], config = {}) {
    super();
    const _config = {
      icon: 'concore',
      label: null,
      view: 'Cards',
      ...config,
    };

    const { icon, label, view } = _config;
    _metatomManager.set(this, new MetatomManager(this, _attrs, 'metatoms'));

    // Try set values in constructor.
    // If throw exception, rollback value to {}
    if (!name || name === '') {
      throw new Error('Moleculoid name cannot be empty!');
    }

    _attrs.set(this, {
      ...this.attributes,
      icon,
      label: label || name,
      name,
      view,
    });

    try {
      this.addMetatoms(metatoms);
    } catch (e) {
      _attrs.set(this, {});
      throw e;
    }
  }

  /**
   * Return class name
   * @return {String}
   */
  get className() {
    return 'Moleculoid';
  }

  getGroup() {
    return this.attributes.group;
  }

  /**
   * Return Moleculoid ID
   * @return {String}
   */
  getId() {
    return this.attributes.name;
  }

  /**
   * Return Moleculoid Title Metatom
   * @return {Metatom}
   */
  getTitle() {
    const metatoms = this.getMetatoms();
    const key = Object.keys(metatoms).shift();

    if (metatoms[key]) {
      return metatoms[key];
    }

    return null;
  }

  /**
   * Add one metatom or a list of metatoms to Moleculoid
   * @method  addMetatoms
   * @param  {Array|Object}  item
   * @return {Moleculoid}
   */
  addMetatoms(item) {
    _metatomManager.get(this).add(item);
    return this;
  }

  /**
   * Destroy this Moleculoid on the server if it was already persisted
   * @method  destroy
   * @return {Promise}
   */
  destroy() {
    const attributesJSON = this.toJSON();
    const attributes = this.attributes;
    const { name } = this.attributes;

    if (!attributes.id) {
      return Promise.reject(`The ${this.className} needs a ID to destroy.`);
    }

    return new Promise((resolve, reject) => {
      Concore.request()
        .delete(`/moleculoid/${name}`, attributesJSON)
        .then(() => {
          delete attributes.id;
          delete attributes.createdAt;
          delete attributes.updatedAt;
          _attrs.set(this, {
            ...attributes
          });

          resolve(this);
        })
        .catch(error => reject(error.response));
    });
  }

  /**
   * Return a List of Metatoms
   * @method getMetatoms
   * @return {Array}
   */
  getMetatoms(name) {
    return _metatomManager.get(this).get(name);
  }

  /**
   * Gets the name of Moleculoid.
   * @method getName
   * @return {String}
   */
  getName() {
    return this.attributes.name;
  }

  /**
   * Gets the label of Moleculoid.
   * @method getLabel
   * @return {String}
   */
  getLabel() {
    return this.attributes.label;
  }

  /**
   * Gets the icon of Moleculoid.
   * @method getIcon
   * @return {String}
   */
  getIcon() {
    return this.attributes.icon;
  }

  /**
   * Gets the visualization of Moleculoid
   * @method  getView
   * @return {String}
   */
  getView() {
    return this.attributes.view;
  }

  /**
   * Try restore Moleculoid in the server
   * @static
   * @method get
   * @param {String} moleculoid
   * @return {Promise}
   */
  restore() {
    const { name } = this.attributes;
    return new Promise((resolve, reject) => {
      const url = `/moleculoid/${name}/restore`;

      Concore.request()
        .patch(url)
        .then(() => resolve(this))
        .catch(error => reject(error));
    });
  }

  /**
   * Remove a Metatom from Metatoms list
   * @method removeMetatoms
   * @param {Object|String} item Metatom instance OR Metatom id
   * @return {Moleculoid}
   */
  removeMetatoms(item) {
    _metatomManager.get(this).remove(item);
    return this;
  }

  /**
   * Save the Object to the server
   * @method save
   * @return {Promise}
   */
  save() {
    const attributesJSON = this.toJSON();
    const attributes = this.attributes;
    const { name } = this.attributes;

    if (!Array.isArray(attributes.metatoms) || attributes.metatoms.length === 0) {
      return Promise.reject('The Moleculoid required metatoms to save.');
    }

    delete attributesJSON.group;

    return new Promise((resolve, reject) => {
      Concore.request()
        .post(`/moleculoid/${name}`, attributesJSON)
        .then(response => {
          const { data } = response;
          const { createdAt, id, updatedAt, group } = data;
          _attrs.set(this, {
            ...attributes,
            createdAt,
            id,
            updatedAt,
            group: group ? Group._inflateGroup(group) : group,
          });

          resolve(this);
        })
        .catch(error => reject(error.response));
    });
  }

  /**
   * Set label attribute to Moleculoid model.
   * @method  setLabel
   * @param {String} value
   * @return {Moleculoid}
   */
  setLabel(value) {
    if (toType(value) !== 'string') {
      throw new TypeError('Label must be a string');
    }

    _attrs.set(this, {
      ...this.attributes,
      label: value
    });

    return this;
  }

  /**
   * Set icon.
   * @method  setIcon
   * @param {String} value
   * @return {Moleculoid}
   */
  setIcon(value) {
    if (toType(value) !== 'string') {
      throw new TypeError('Icon must be a string');
    }

    _attrs.set(this, {
      ...this.attributes,
      icon: value
    });

    return this;
  }

  /**
   * Set view.
   * @method  setView
   * @param {String} value
   * @return {Moleculoid}
   */
  setView(value) {
    _attrs.set(this, {
      ...this.attributes,
      view: value
    });

    return this;
  }

  /**
   * Returns a JSON version of the object
   * @method  toJSON
   * @return {Object} [description]
   */
  toJSON() {
    const attrsJSON = {};

    Object.keys(this.attributes).forEach(attr => {
      let value = this.attributes[attr];
      if (value instanceof ACL) {
        value = value.toJSON();
      }
      attrsJSON[attr] = value;
    });

    return {
      ...attrsJSON,
      metatoms: _metatomManager.get(this).toJSON()
    };
  }

  /**
   * Get Moleculoid schema from the server
   * @static
   * @method get
   * @param {String} moleculoid
   * @return {Promise}
   */
  static get(moleculoid) {
    return new Promise((resolve, reject) => {
      let url = '/moleculoid';

      if (moleculoid) {
        url += `/${moleculoid}`;
      }

      Concore.request()
        .get(url)
        .then(response => {
          const { data } = response;
          const { results } = data;

          if (results) {
            return resolve(results.map(schema =>
              this._inflateMoleculoid(schema)
            ));
          }

          return resolve(this._inflateMoleculoid(data));
        })
        .catch(error => reject(error));
    });
  }

  /**
   * Try restore Moleculoid in the server
   * @static
   * @method get
   * @param {String} moleculoid
   * @return {Promise}
   */
  static restore(moleculoid) {
    return new Promise((resolve, reject) => {
      const url = `/moleculoid/${moleculoid}/restore`;

      Concore.request()
        .patch(url)
        .then(() => Moleculoid.get(moleculoid))
        .then(m => resolve(m))
        .catch(error => reject(error));
    });
  }

  /**
   * @ignore
   */
  static _inflateMoleculoid(moleculoid, shouldCreateReference = true) {
    const newMoleculoid = { ...moleculoid };
    const { icon, label, group, metatoms, name, view } = newMoleculoid;
    const references = newMoleculoid.references || {};
    delete newMoleculoid.references;
    const metatomsList = metatoms.map(
      metatom => MetaInflator.inflate(metatom)
    );

    const metatomsReference = metatomsList
      .filter(metatom => metatom instanceof Reference);

    if (shouldCreateReference && metatomsReference.length) {
      Object.keys(references).forEach(moleculoidName => {
        references[moleculoidName] = Moleculoid
          ._inflateMoleculoid(references[moleculoidName]);
      });

      metatomsReference.forEach(metatom => {
        const moleculoidName = metatom.getMoleculoid();

        if (moleculoidName === name) {
          references[name] = Moleculoid._inflateMoleculoid(newMoleculoid, false);
        }

        if (!{}.hasOwnProperty.call(references, moleculoidName)) {
          return;
        }

        MetatomState(metatom).set('reference', references[moleculoidName]);
      });
    }

    const instance = new Moleculoid(name, metatomsList, {
      label, icon, view
    });
    _attrs.set(instance, {
      ...instance.attributes,
      group,
      createdAt: newMoleculoid.createdAt,
      id: newMoleculoid.id,
      updatedAt: newMoleculoid.updatedAt,
      ACL: new ACL(newMoleculoid.ACL),
    });

    return instance;
  }
}
