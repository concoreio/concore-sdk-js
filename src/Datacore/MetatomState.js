const weakMap = new WeakMap();

const MetatomState = context => {
  if (!weakMap.get(context)) {
    weakMap.set(context, {});
  }

  return {
    attributes() {
      return weakMap.get(context);
    },

    get(key) {
      const attrs = weakMap.get(context);
      return attrs[key];
    },

    set(key, value) {
      const attrs = weakMap.get(context);
      weakMap.set(context, {
        ...attrs,
        [key]: value
      });

      return context;
    }
  };
};

/**
 * @ignore
 */
export default MetatomState;
