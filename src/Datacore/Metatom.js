import { slugify, solveConfigId, toType, ucfirst } from '../helpers';

import MetatomState from './MetatomState';
/**
 * Abstract Metatom class.
 *
 * @ignore
 */
export default class Metatom {

  /**
   * @param  {String} label
   * @param  {Object} _config
   * @param  {String} _id
   * @return {Metatom}
   *
   * @throws {TypeError} when given id is invalid
   * @throws {TypeError} when given label dont build a valid id
   */
  constructor(label, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);

    if (!/^[A-Za-z][0-9A-Za-z_]*$/.test(id)) {
      throw new TypeError('Metatom ID is invalid.');
    }

    if ((label && !id) && !slugify(label)) {
      throw new TypeError('For this label we cant build an id. Give a custom id.');
    }

    const defaultConfig = {
      required: true,
      order: 0,
      columns: 4
    };

    MetatomState(this).set('id', id || slugify(label));
    MetatomState(this).set('metatomType', this.className);
    this.setLabel(label);
    this._setConfig({
      ...defaultConfig,
      ...config
    });
  }

  /**
   * Return an Object Representation of Metatom
   * @property attributes
   * @return {Object}
   */
  get attributes() {
    return MetatomState(this).attributes();
  }

  /**
   * Return the columns number
   * @method getColumns
   * @return {Integer}
   */
  getColumns() {
    return MetatomState(this).get('columns');
  }

  /**
   * Return the Metatom id
   * @method getId
   * @return {String}
   */
  getId() {
    return MetatomState(this).get('id');
  }

  /**
   * Return the Metatom Label
   * @method getLabel
   * @return {String}
   */
  getLabel() {
    return MetatomState(this).get('label');
  }

  /**
   * Return the Metatom Type
   * @method getMetatomType
   * @return {String}
   */
  getMetatomType() {
    return MetatomState(this).get('metatomType');
  }

  /**
   * Return the Metatom Order
   * @method getOrder
   * @return {Number}
   */
  getOrder() {
    return MetatomState(this).get('order');
  }

  /**
   * Get the Metatom Input Mask
   * @method getMask
   * @return {String}
   */
  getMask() {
    return MetatomState(this).get('mask');
  }

  /**
   * Return the Metatom Label
   * @method getLabel
   * @return {Boolean}
   */
  getRequired() {
    return MetatomState(this).get('required');
  }

  /**
   * Set the Metatom Columns
   * @method setColumns
   * @param {String} columns
   * @return {Metatom}
   *
   * @throws {TypeError} when columns not be a number
   */
  setColumns(columns = 4) {
    if (toType(columns) !== 'number') {
      throw new TypeError('Columns must be a number of 1 until 4.');
    }

    return MetatomState(this).set('columns', columns);
  }

  /**
   * Set the Metatom Label
   * @method setLabel
   * @param {String} label
   * @return {Metatom}
   *
   * @throws {TypeError} when label not be a string
   */
  setLabel(label) {
    if (toType(label) !== 'string') {
      throw new TypeError('Label must be a string.');
    }

    return MetatomState(this).set('label', label);
  }

  /**
   * Set the Metatom Order
   * @method setOrder
   * @param {Number} order
   * @return {Metatom}
   *
   * @throws {TypeError} when order not be a number
   */
  setOrder(order) {
    if (toType(order) !== 'number') {
      throw new TypeError('Order must be a number.');
    }
    return MetatomState(this).set('order', order);
  }

  /**
   * Set the Metatom Input Mask
   * @method setMask
   * @param {String} mask
   * @return {Metatom}
   *
   * @throws {TypeError} when mask not be a string
   */
  setMask(mask) {
    if (toType(mask) !== 'string') {
      throw new TypeError('Mask must be a string.');
    }
    return MetatomState(this).set('mask', mask);
  }

  /**
   * Set the Metatom Required
   * @method setRequired
   * @param {Boolean} required
   * @return {Metatom}
   *
   * @throws {TypeError} when required not be a boolean
   */
  setRequired(required) {
    if (toType(required) !== 'boolean') {
      throw new TypeError('Required must be a boolean.');
    }

    return MetatomState(this).set('required', required);
  }

  /**
   * Returns a JSON version of the object
   * @method  toJSON
   * @return {Object} [description]
   */
  toJSON() {
    const attributes = { ...MetatomState(this).attributes() };
    delete attributes.reference;

    return attributes;
  }

  _setConfig(config = {}) {
    if (toType(config) !== 'object') {
      throw new TypeError('Config must be object');
    }

    Object.keys(config).forEach(key => {
      const methodName = `set${ucfirst(key)}`;

      if (toType(this[methodName]) !== 'function') {
        return false;
      }

      this[methodName](config[key]);
    });
  }
}
