import ACL from './ACL';
import AtomFile from './AtomFile';
import Auth from './Auth';
import Group from './Group';
import MetatomType from './MetatomType';
import Molecule from './Molecule';
import Moleculoid from './Moleculoid';
import MoleculeQuery from './MoleculeQuery';

/**
 * Datacore SDK
 */
export default class Datacore {
  /**
   * Class  Datacore.ACL
   */
  static ACL = ACL;
  /**
   * Class  Datacore.Auth
   */
  static Auth = Auth;
  /**
   * Class  Datacore.AtomFile
   */
  static AtomFile = AtomFile;
  /**
   * Class  Datacore.Group
   */
  static Group = Group;
  /**
   * Class  Datacore.MetatomType
   */
  static MetatomType = MetatomType;
  /**
   * Class  Datacore.Molecule
   */
  static Molecule = Molecule;
  /**
   * Class  Datacore.Moleculoid
   */
  static Moleculoid = Moleculoid;
  /**
   * Class  Datacore.MoleculeQuery
   */
  static MoleculeQuery = MoleculeQuery;
}
