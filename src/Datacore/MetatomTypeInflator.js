import MetatomType from './MetatomType';

/**
 * @ignore
 */
export default class MetatomTypeInflator {
  static inflate(metatom) {
    const object = { ...metatom };
    const type = object.metatomType;

    const methodInflator = `inflate${type}`;

    if (typeof this[methodInflator] === 'function') {
      return this[methodInflator](object);
    }

    return this.defaultInflator(object);
  }

  static defaultInflator(object) {
    const { label, metatomType } = object;
    const config = { ...object };
    delete config.label;
    delete config.renderType;
    delete config.metatomType;

    return new MetatomType[metatomType](label, config);
  }

  static inflatorRenderType(object) {
    const { label, metatomType } = object;
    const config = { ...object };
    delete config.label;
    delete config.metatomType;

    return new MetatomType[metatomType](label, config);
  }

  static inflateChoice(object) {
    const { label, metatomType, options } = object;
    const config = { ...object };
    delete config.label;
    delete config.metatomType;
    delete config.options;

    return new MetatomType[metatomType](label, options, config);
  }

  static inflateReference(object) {
    const { label, metatomType, moleculoid } = object;
    const config = { ...object };
    delete config.label;
    delete config.metatomType;
    delete config.moleculoid;
    delete config.renderType;

    return new MetatomType[metatomType](label, moleculoid, config);
  }

  static inflateText(object) {
    return this.inflatorRenderType(object);
  }
}
