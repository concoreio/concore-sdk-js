import { toType, solveConfigId } from '../../helpers';
import Metatom from '../Metatom';
import MetatomState from '../MetatomState';

/**
* MetatomType Currency.
*
* @example
* // with default configuration
* const salary = new Currency('Salary');
* // { id: 'salary', label: 'Salary', currency: 'USD', options: [], required: true }
*
* // Config currency = 'BRL' and some acceptable options
* const salary = new Currency('Salary', {
*   currency: 'BRL',
*   options: ['USD', 'EUR', 'GBP']
* });
*
* // { id: 'salary', label: 'Salary', currency: 'BRL', options: [options], required: true }
*
*/
export default class Currency extends Metatom {

  /**
   * @param  {String}  [label]
   * @param  {Object|String} [config|id]
   * @param  {String}  [id=null]
   * @return {Currency}
   */
  constructor(label, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      currency: 'USD',
      ...config
    };
    super(label, newConfig, id);
    MetatomState(this).set('renderType', 'currency');
  }

  get className() {
    return 'Currency';
  }

  /**
   * Add option to list of options
   *
   * @example
   * salary.addOption('JPY');
   *
   * @return {Currency}
  */
  addOption(option) {
    if (!Array.isArray(option)) {
      throw new TypeError('Options must be an array.');
    }
    const options = this.getOptions();
    options.push(option);
    return this.setOptions(options);
  }

  /**
   * Add a list of options to list options
   * @example
   * salary.addOptions(['AUD', 'CAD']);
   *
   * @return {Currency}
  */
  addOptions(options) {
    if (!Array.isArray(options)) {
      throw new TypeError('Options must be an array.');
    }
    options.forEach(option => this.addOption(option));
    return this;
  }

  /**
   * Get currency config
   * @return {string}
  */
  getCurrency() {
    return MetatomState(this).get('currency');
  }

  /**
   * Get a list of options
   * @return {array}
  */
  getOptions() {
    return MetatomState(this).get('options');
  }

  /**
   * Remove an option from list of options
   * @example
   * salary.removeOptions('AUD');
   *
   * @return {Currency}
  */
  removeOption(option) {
    const options = this.getOptions().filter(opt => opt !== option);
    return this.setOptions(options);
  }

  /**
   * Remove a list of options from list of options
   * @example
   * salary.removeOptions(['JPY', 'CAD']);
   *
   * @return {Currency}
  */
  removeOptions(options) {
    options.forEach(option => this.removeOption(option));
    return this;
  }

  /**
   * Set currency config
   * @return {Currency}
  */
  setCurrency(currency) {
    if (toType(currency) !== 'string') {
      throw new TypeError('Currency must be a string.');
    }
    MetatomState(this).set('currency', currency);
    return this;
  }

  /**
   * Set a list of options
   * @return {Currency}
  */
  setOptions(options) {
    if (!Array.isArray(options)) {
      throw new TypeError('Options must be an array.');
    }

    const valid = options.every(option => toType(option) === 'string');

    if (!valid) {
      throw new TypeError('Options must be an array of string.');
    }

    MetatomState(this).set('options', options);
    return this;
  }
}
