import { solveConfigId, toType } from '../../helpers';
import Metatom from '../Metatom';
import MetatomState from '../MetatomState';

/**
* MetatomType DateTime.
*
* @example
* // with default configuration
* const birth = new DateTime('Birth');
* // { id: 'birth', label: 'Birth', format: null, ype: 'datetime', required: true }
*
* // Config format = 'dd/mm/YYYY' and type = date
* const birth = new DateTime('Birth', {
*   required: false,
*   type: 'date',
*   format: 'dd/mm/YYYY',
* });
*/
export default class DateTime extends Metatom {

  /**
   * @param  {String}  [label]
   * @param  {Object|String} [config|id]
   * @param  {String}  [id=null]
   * @return {Currency}
   */
  constructor(label, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      format: null,
      type: 'datetime',
      ...config
    };
    super(label, newConfig, id);
    MetatomState(this).set('renderType', 'datetime');
  }

  get className() {
    return 'DateTime';
  }

  /**
   * Get format
   * @return {string}
  */
  getFormat() {
    return MetatomState(this).get('format');
  }
  /**
   * Get type
   * @return {string}
  */
  getType() {
    return MetatomState(this).get('type');
  }
  /**
   * Set format
   * @property format
   * @return {this}
  */
  setFormat(format) {
    if (toType(format) !== 'string') {
      return new TypeError('Format must be a string');
    }

    MetatomState(this).set('format', format);
    return this;
  }
  /**
   * Set type
   * @property type
   * @return {this}
  */
  setType(type) {
    const allowed = ['date', 'datetime', 'time'];

    if (!allowed.includes(type)) {
      throw new TypeError(`DateTime type is not permited. Just [${allowed.join(', ')}] is permited.`);
    }

    MetatomState(this).set('type', type);
  }
}
