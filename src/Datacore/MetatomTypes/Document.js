import { toType, solveConfigId } from '../../helpers';
import File from './File';
import MetatomState from '../MetatomState';

const allowedDocuments = [
  'doc', 'dot', // Microsoft Word 6.0/95/97/2000/XP
  'xml', // Microsoft Word 2003 XML, DocBook
  'docx', 'docm', 'dotx', 'dotm', // Microsoft Word 2007 XML
  'wpd', // WordPerfect Document
  'wps', // WPS 2000/Office 1.0
  'rtf', 'txt', 'csv',
  'sdw', 'sgl', 'vor', // StarWriter formats
  'uot', 'uof', // Unified Office Format text
  'jtd', 'jtt', // Ichitaro 8/9/10/11
  'hwp', // Hangul WP 97
  '602', 'txt', // T602 Document
  'pdb', // AportisDoc (Palm)
  'psw', // Pocket Word
  'xls', 'xlw', 'xlt', // Microsoft Excel 97/2000/XP
  'xls', 'xlw', 'xlt', // Microsoft Excel 4.x–5.0/95
  'xml', // Microsoft Excel 2003 XML
  'xlsx', 'xlsm', 'xltx', 'xltm', // Microsoft Excel 2007 XML
  'xlsb', // Microsoft Excel 2007 binary
  'wk1', 'wks', '123', // Lotus 1-2-3
  'dif', // Data Interchange Format
  'sdc', 'vor', // StarCalc formats
  'dbf', // dBASE
  'slk', // SYLK
  'uos', 'uof', // Unified Office Format spreadsheet
  'htm', 'html',
  'pxl', // Pocket Excel
  'wb2', // Quattro Pro 6.0,
  'ppt', 'pps', 'pot', // Microsoft PowerPoint 97/2000/XP
  'pptx', 'pptm', 'potx', 'potm', // Microsoft PowerPoint 2007
  'sda', 'sdd', 'sdp', 'vor', // StarDraw and StarImpress
  'uop', 'uof', // Unified Office Format presentation
  'cgm', // CGM – Computer Graphics Metafile
  'pdf', // Portable Document Format
];
/**
 * Document class.
 *
 * @module Datacore
 * @class Document
 * @constructor
 * @param  {String}  [label]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {Document}
*/
export default class Document extends File {
  constructor(label, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      extensions: allowedDocuments,
      ...config
    };
    super(label, newConfig, id);
  }

  get className() {
    return 'Document';
  }

  /**
   * Set extensions
   * @property extensions
   * @return {this}
  */
  setExtensions(extensions) {
    if (toType(extensions) !== 'array') {
      throw new TypeError('Extensions must be an array of strings');
    }

    if (!extensions.every(ext => allowedDocuments.includes(ext))) {
      throw new TypeError(`Extensions allowed: [${allowedDocuments}]`);
    }

    MetatomState(this).set('extensions', extensions);
    return this;
  }
}
