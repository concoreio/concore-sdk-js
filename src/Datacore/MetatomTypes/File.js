import { toType, solveConfigId } from '../../helpers';
import Metatom from '../Metatom';
import MetatomState from '../MetatomState';
/**
 * File class.
 *
 * @module Datacore
 * @class File
 * @constructor
 * @param  {String}  [label]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {File}
*/
export default class File extends Metatom {
  constructor(label, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      extensions: '*',
      multiple: false,
      ...config
    };
    super(label, newConfig, id);

    if (this.className !== 'Image') {
      MetatomState(this).set('renderType', 'upload');
    }
  }

  get className() {
    return 'File';
  }

  /**
   * Get extensions
   * @return {array}
  */
  getExtensions() {
    return MetatomState(this).get('extensions');
  }
  /**
   * Get multiple
   * @return {boolean}
  */
  getMultiple() {
    return MetatomState(this).get('multiple');
  }
  /**
   * Set extensions
   * @property extensions
   * @return {this}
  */
  setExtensions(extensions = '*') {
    if (toType(extensions) === 'array') {
      if (!extensions.every(ext => toType(ext) === 'string')) {
        throw new TypeError('Extensions must be an array of strings');
      }
    } else if (extensions !== '*') {
      throw new TypeError('Extensions is invalid');
    }

    MetatomState(this).set('extensions', extensions);
    return this;
  }
  /**
   * Set multiple
   * @property multiple
   * @return {this}
  */
  setMultiple(multiple = false) {
    if (toType(multiple) !== 'boolean') {
      throw new TypeError('Multiple must be a boolean');
    }
    MetatomState(this).set('multiple', multiple);
    return this;
  }
}
