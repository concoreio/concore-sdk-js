import Metatom from '../Metatom';
import MetatomState from '../MetatomState';
/**
 * List class.
 *
 * @ignore
 *
 * @module Datacore
 * @class List
 * @constructor
 * @param  {String}  [label]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {List}
*/
export default class List extends Metatom {
  constructor(label, _config = {}, _id = null) {
    super(label, _config, _id);
    MetatomState(this).set('renderType', 'list');
  }

  get className() {
    return 'List';
  }
}
