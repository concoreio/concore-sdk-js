import Text from './Text';
import MetatomState from '../MetatomState';
/**
 * Email class.
 *
 * @module Datacore
 * @class Email
 * @constructor
 * @param  {String}  [label]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {Email}
*/
export default class Email extends Text {
  constructor(label, _config = {}, _id = null) {
    super(label, _config, _id);
    MetatomState(this).set('renderType', 'email');
  }

  get className() {
    return 'Email';
  }
}
