import { toType, solveConfigId } from '../../helpers';
import File from './File';
import MetatomState from '../MetatomState';

const allowedImages = ['gif', 'jpg', 'png'];
/**
 * Image class.
 *
 * @module Datacore
 * @class Image
 * @constructor
 * @param  {String}  [label]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {Image}
*/
export default class Image extends File {
  constructor(label, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      extensions: allowedImages,
      renderType: 'upload',
      ...config
    };
    super(label, newConfig, id);
  }

  get className() {
    return 'Image';
  }

  /**
   * Get render type config value
   * @return {string}
  */
  getRenderType() {
    return MetatomState(this).get('renderType');
  }

  /**
   * Set extensions
   * @property extensions
   * @return {this}
  */
  setExtensions(extensions) {
    if (toType(extensions) !== 'array') {
      throw new TypeError('Extensions must be an array of strings');
    }

    if (!extensions.every(ext => allowedImages.includes(ext))) {
      throw new TypeError(`Extensions allowed: [${allowedImages}]`);
    }

    MetatomState(this).set('extensions', extensions);
    return this;
  }

  /**
   * Set render type. Accept 'upload', 'signature'
   * @return {Choice}
  */
  setRenderType(renderType) {
    const allowed = ['upload', 'signature'];
    if (!allowed.includes(renderType)) {
      throw new TypeError(`Render type not allowed. Just [${allowed.join(', ')}]`);
    }

    MetatomState(this).set('renderType', renderType);
    return this;
  }
}
