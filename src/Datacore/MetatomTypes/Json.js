import Metatom from '../Metatom';
import MetatomState from '../MetatomState';
/**
 * Json class.
 *
 * @module Datacore
 * @class Json
 * @constructor
 * @param  {String}  [label]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {Json}
*/
export default class Json extends Metatom {
  constructor(label, _config = {}, _id = null) {
    super(label, _config, _id);
    MetatomState(this).set('renderType', 'json');
  }

  get className() {
    return 'Json';
  }
}
