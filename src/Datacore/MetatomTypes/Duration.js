import { solveConfigId } from '../../helpers';
import Metatom from '../Metatom';
import MetatomState from '../MetatomState';

const fieldsAllowed = ['days', 'hours', 'minutes', 'seconds'];
/**
 * Duration class.
 *
 * @module Datacore
 * @class Duration
 * @constructor
 * @param  {String}  [label]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {Duration}
*/
export default class Duration extends Metatom {

  constructor(label, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      fields: fieldsAllowed,
      ...config
    };
    super(label, newConfig, id);
    MetatomState(this).set('renderType', 'duration');
  }

  get className() {
    return 'Duration';
  }

  /**
   * Get fields
   * @return {array}
  */
  getFields() {
    return MetatomState(this).get('fields');
  }
  /**
   * Set fields
   * @property fields
   * @return {this}
  */
  setFields(fields) {
    if (!Array.isArray(fields)) {
      throw new TypeError('Fields must be an array.');
    }

    const valid = fields.every(field => fieldsAllowed.includes(field));

    if (!valid) {
      throw new TypeError(`Fields just permit [${fieldsAllowed.join(', ')}].`);
    }

    MetatomState(this).set('fields', fields);
  }
}
