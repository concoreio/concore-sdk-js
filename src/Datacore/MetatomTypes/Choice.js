import { toType, solveConfigId } from '../../helpers';
import Metatom from '../Metatom';
import MetatomState from '../MetatomState';

/**
 * MetatomType Choice.
 *
 * @example
 * // with default configuration
 * const gender = new Choice('Gender', [{
 *  value: 'M', label: 'Male'
 * }, {
 *  value: 'F', label: 'Female'
 * }]);
 * // { id: 'gender', 'options': [options], multiple: false, renderType: 'select', required: true }
 *
 * // configuration options
 * const gender = new Choice('Gender', [{
 *  value: 'M', label: 'Male'
 * }, {
 *  value: 'F', label: 'Female'
 * }], {
 *  required: true,
 *  multiple: false,
 *  renderType: 'select' // Accept checkbox, radio, select
 * });
 *
 */
export default class Choice extends Metatom {

  /**
   * @param  {String}  label
   * @param  {Array}   options
   * @param  {Object|String} [config|id]
   * @param  {String}  [id=null]
   * @return {Choice}
   */
  constructor(label, options, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      options,
      multiple: false,
      renderType: 'select',
      ...config
    };
    super(label, newConfig, id);
  }

  get className() {
    return 'Choice';
  }

  /**
   * Add an option to list of options
   * @example
   * gender.addOption({
   *  value: 'O', label: 'Non-informed'
   * });
   *
   * @return {Choice}
  */
  addOption(option) {
    const options = this.getOptions();
    options.push(option);
    return this.setOptions(options);
  }

  /**
   * Add a list of options to list of options
   * @example
   * gender.addOptions([{
   *  value: 'O', label: 'Non-informed'
   * }]);
   *
   * @return {Choice}
  */
  addOptions(options) {
    options.forEach(option => this.addOption(option));
    return this;
  }

  /**
   * Get multiple config value
   * @return {boolean}
  */
  getMultiple() {
    return MetatomState(this).get('multiple');
  }

  /**
   * Get the list of options
   * @return {array}
  */
  getOptions() {
    return MetatomState(this).get('options');
  }

  /**
   * Get render type config value
   * @return {string}
  */
  getRenderType() {
    return MetatomState(this).get('renderType');
  }

  /**
   * Remove one option from list of options
   * @example
   * gender.removeOption({
   *  value: 'O', label: 'Non-informed'
   * });
   *
   * // or remove by value
   *
   * gender.removeOption('O');
   *
   * @return {Choice}
  */
  removeOption(option) {
    const options = this.getOptions().filter(opt => {
      if (toType(option) === 'object') {
        return JSON.stringify(option) !== JSON.stringify(opt);
      }

      return option !== opt.value;
    });

    return this.setOptions(options);
  }

  /**
   * Remove a list of options from list of options
   * @example
   * gender.removeOptions([{
   *  value: 'O', label: 'Non-informed'
   * }]);
   *
   * @return {Choice}
  */
  removeOptions(options) {
    options.forEach(option => this.removeOption(option));
    return this;
  }

  /**
   * Set multiple. Allow choice accepts more then one value
   * @return {Choice}
  */
  setMultiple(multiple) {
    if (toType(multiple) !== 'boolean') {
      throw new TypeError('Multiple must be a boolean');
    }
    MetatomState(this).set('multiple', multiple);
    return this;
  }

  /**
   * Set an array of Options.
   * @return {Choice}
  */
  setOptions(options) {
    if (!Array.isArray(options) || options.length === 0) {
      throw new TypeError('Options must be an array of option and cant be empty.');
    }

    const valid = options.every(option => toType(option) === 'object' &&
        ({}.hasOwnProperty.call(option, 'label') && {}.hasOwnProperty.call(option, 'value'))
    );

    if (!valid) {
      throw new TypeError('Option must be an object with value and label keys.');
    }

    MetatomState(this).set('options', options);
    return this;
  }

  /**
   * Set render type. Accept 'checkbox', 'radio', 'select'
   * @return {Choice}
  */
  setRenderType(renderType) {
    const allowed = ['checkbox', 'radio', 'select'];
    if (!allowed.includes(renderType)) {
      throw new TypeError(`Render type not allowed. Just [${allowed.join(', ')}]`);
    }

    MetatomState(this).set('renderType', renderType);
    return this;
  }
}
