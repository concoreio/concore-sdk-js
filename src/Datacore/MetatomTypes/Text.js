import { solveConfigId, toType } from '../../helpers';
import Metatom from '../Metatom';
import MetatomState from '../MetatomState';

/**
 * Text class.
 *
 * @module Datacore
 * @class Text
 * @constructor
 * @param  {String}  [label]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {Text}
*/
export default class Text extends Metatom {

  constructor(label, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      renderType: 'text',
      validation: null,
      ...config
    };
    super(label, newConfig, id);
  }

  get className() {
    return 'Text';
  }

  /**
   * Get validation
   * @property validation
   * @return {pattern}
  */
  getValidation() {
    return MetatomState(this).get('validation');
  }
  /**
   * Get renderType
   * @return {string}
  */
  getRenderType() {
    return MetatomState(this).get('renderType');
  }
  /**
   * Get rows
   * @return {number}
  */
  getRows() {
    return MetatomState(this).get('rows');
  }
  /**
   * Set renderType
   * @property renderType
   * @return {this}
  */
  setRenderType(renderType) {
    const allowed = ['text', 'textarea'];
    if (!allowed.includes(renderType)) {
      throw new TypeError(`Render type not allowed. Just [${allowed.join(', ')}]`);
    }

    MetatomState(this).set('renderType', renderType);
    return this;
  }
  /**
   * Set rows
   * @property rows
   * @return {this}
  */
  setRows(rows) {
    if (toType(rows) !== 'number') {
      throw new TypeError('Rows must be number');
    }
    MetatomState(this).set('rows', rows);
    return this;
  }
  /**
   * Set validation
   * @property validation
   * @return {this}
  */
  setValidation(validation) {
    MetatomState(this).set('validation', validation);
    return this;
  }
}
