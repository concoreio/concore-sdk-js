import { toType, solveConfigId } from '../../helpers';
import Text from './Text';
import MetatomState from '../MetatomState';
/**
 * Url class.
 *
 * @module Datacore
 * @class Url
 * @constructor
 * @param  {String}  [label]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {Url}
*/
export default class Url extends Text {
  constructor(label, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      multiple: false,
      ...config
    };
    super(label, newConfig, id);
    MetatomState(this).set('renderType', 'url');
  }

  get className() {
    return 'Url';
  }

  /**
   * Get multiple
   * @return {boolean}
  */
  getMultiple() {
    return MetatomState(this).get('multiple');
  }

  /**
   * Set multiple
   * @property multiple
   * @return {this}
  */
  setMultiple(multiple = false) {
    if (toType(multiple) !== 'boolean') {
      throw new TypeError('Multiple must be a boolean');
    }
    MetatomState(this).set('multiple', multiple);
    return this;
  }
}
