import Metatom from '../Metatom';
import MetatomState from '../MetatomState';
import { solveConfigId, toType } from '../../helpers';

 /**
  * Password class.
  *
  * @module Datacore
  * @class Password
  * @constructor
  * @param  {String}  [label]
  * @param  {Object|String} [config|id]
  * @param  {String}  [id=null]
  * @return {Password}
  */
export default class Password extends Metatom {
  constructor(label, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      algorithm: 'md5',
      saltRounds: 0,
      ...config
    };
    super(label, newConfig, id);
    MetatomState(this).set('renderType', 'password');
  }

  get className() {
    return 'Password';
  }

  /**
   * Get algorithm
   * @return {string}
  */
  getAlgorithm() {
    return MetatomState(this).get('algorithm');
  }
  /**
   * get saltRounds
   * @return {number}
  */
  getSaltRounds() {
    return MetatomState(this).get('saltRounds');
  }
  /**
   * Get validation
   * @property validation
   * @return {pattern}
  */
  getValidation() {
    return MetatomState(this).get('validation');
  }
  /**
   * Set algorithm
   * @property algorithm
   * @return {this}
  */
  setAlgorithm(algorithm) {
    if (toType(algorithm) !== 'string') {
      return new TypeError('Algorithm must be string ');
    }

    const allowed = ['md5', 'textPlain', 'sha1', 'sha256', 'bcrypt'];
    if (!allowed.includes(algorithm)) {
      throw new TypeError(`algorithm type not allowed. Just [${allowed.join(', ')}]`);
    }
    MetatomState(this).set('algorithm', algorithm);
    return this;
  }
  /**
   * Set saltRounds
   * @property saltRounds
   * @return {this}
  */
  setSaltRounds(saltRounds) {
    if (toType(saltRounds) !== 'number') {
      return new TypeError('SaltRounds must be a number');
    }

    MetatomState(this).set('saltRounds', saltRounds);
    return this;
  }
  /**
   * Set validation
   * @property validation
   * @return {this}
  */
  setValidation(validation) {
    MetatomState(this).set('validation', validation);
    return this;
  }
}
