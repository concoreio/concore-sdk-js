import Metatom from '../Metatom';
import MetatomState from '../MetatomState';
/**
 * NumberType class.
 *
 * @module Datacore
 * @class NumberType
 * @constructor
 * @param  {String}  [label]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {NumberType}
*/
export default class NumberType extends Metatom {
  constructor(label, _config = {}, _id = null) {
    super(label, _config, _id);
    MetatomState(this).set('renderType', 'number');
  }

  get className() {
    return 'NumberType';
  }
}
