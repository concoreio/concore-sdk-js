import Text from './Text';
import MetatomState from '../MetatomState';
/**
 * Location class.
 *
 * @module Datacore
 * @class Location
 * @constructor
 * @param  {String}  [label]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {Location}
*/
export default class Location extends Text {
  constructor(label, _config = {}, _id = null) {
    super(label, _config, _id);
    MetatomState(this).set('renderType', 'location');
  }

  get className() {
    return 'Location';
  }
}
