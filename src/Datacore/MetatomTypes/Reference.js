import { solveConfigId, toType } from '../../helpers';
import Metatom from '../Metatom';
import MetatomState from '../MetatomState';
/**
 * Reference class.
 *
 * @module Datacore
 * @class Reference
 * @constructor
 * @param  {String}  [label]
 * @param  {String} [moleculoid]
 * @param  {Object|String}  [config|id]
 * @param  {String} [id=null]
 * @return {Reference}
*/
export default class Reference extends Metatom {
  constructor(label, moleculoid, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      multiple: true,
      moleculoid,
      ...config,
    };
    super(label, newConfig, id);
    MetatomState(this).set('renderType', 'reference');
  }

  get className() {
    return 'Reference';
  }

  /**
   * Get labelId
   * @return {string}
  */
  getLabelId() {
    return MetatomState(this).get('labelId');
  }
  /**
   * Get moleculoid
   * @return {string}
  */
  getMoleculoid() {
    return MetatomState(this).get('moleculoid');
  }
  /**
   * Get multiple
   * @return {boolean}
  */
  getMultiple() {
    return MetatomState(this).get('multiple');
  }
  /**
   * Set labelId
   * @property id
   * @return {this}
  */
  setLabelId(id) {
    if (!/^[A-Za-z][0-9A-Za-z_]*$/.test(id)) {
      throw new TypeError('Label ID is invalid.');
    }
    MetatomState(this).set('labelId', id);
    return this;
  }
  /**
   * Set moleculoid
   * @property moleculoid
   * @return {this}
  */
  setMoleculoid(moleculoid) {
    if (!/^[a-zA-Z][_a-zA-Z0-9]*$/.test(moleculoid)) {
      throw new TypeError('Moleculoid name is invalid!');
    }

    MetatomState(this).set('moleculoid', moleculoid);
    return this;
  }
  /**
   * Set multipe
   * @property multiple
   * @return {this}
  */
  setMultiple(multiple) {
    if (toType(multiple) !== 'boolean') {
      throw new TypeError('Multiple must be a boolean');
    }
    MetatomState(this).set('multiple', multiple);
    return this;
  }
  /**
   * Get reference
   * @return {Moleculoid}
   */
  getReference() {
    return MetatomState(this).get('reference');
  }
}
