import Metatom from '../Metatom';
import MetatomState from '../MetatomState';
import { solveConfigId } from '../../helpers';

/**
* MetatomType RouteLocation.
*
* @example
* // with default configuration
* const route = new RouteLocation('Home');
* // { id: 'home', label: 'Home', language: 'en-US', mode: 'driving', units: 'metric' }
*
* // Config RouteLocation and some acceptable options
* const routeLocation = new RouteLocation('Home', {
*   language: 'pt-BR',
*   origin: { label: 'Origem'},
*   destination: { label: 'Destino'},
*   units: 'metric',
*   mode: 'bicycling'
* });
*/
export default class RouteLocation extends Metatom {
  constructor(label, _config = {}, _id = null) {
    const { config, id } = solveConfigId(_config, _id);
    const newConfig = {
      destination: {
        label: 'Destination',
      },
      language: 'en-US',
      mode: 'driving',
      origin: {
        label: 'Origin',
      },
      units: 'metric',
      ...config
    };
    super(label, newConfig, id);
    MetatomState(this).set('renderType', 'route');
  }

  /**
   * Get class name
   * @return {string}
   */
  get className() {
    return 'RouteLocation';
  }

  /**
   * Get origin config
   * @return {object}
   */
  getOrigin() {
    return MetatomState(this).get('origin');
  }

  /**
   * Set origin config
   * @param {RouteLocation}
   */
  setOrigin(origin) {
    this._validateDirectionObject(origin);

    MetatomState(this).set('origin', origin);
    return this;
  }

  /**
   * Get destination config
   * @return {object}
   */
  getDestination() {
    return MetatomState(this).get('destination');
  }

  /**
   * Set destination config
   * @param {RouteLocation}
   */
  setDestination(destination) {
    this._validateDirectionObject(destination);

    MetatomState(this).set('destination', destination);
    return this;
  }

  /**
   * Get units config
   * @return {string}
   */
  getUnits() {
    return MetatomState(this).get('units');
  }

  /**
   * Set units config
   * @param {RouteLocation]
   */
  setUnits(units) {
    MetatomState(this).set('units', units);
    return this;
  }

  /**
   * Get mode config - Default: driving
   * @return {string}
   */
  getMode() {
    return MetatomState(this).get('mode');
  }

  /**
   * Set mode config
   * @param {RouteLocation}
   */
  setMode(mode) {
    MetatomState(this).set('mode', mode);
    return this;
  }

  /**
   * Get language config - DEFAULT: en-US
   * @return {string}
   */
  getLanguage() {
    return MetatomState(this).get('language');
  }

  /**
   * Set language config
   * @param {RouteLocation}
   */
  setLanguage(language) {
    MetatomState(this).set('language', language);
    return this;
  }

  _validateDirectionObject(direction) {
    if (!{}.hasOwnProperty.call(direction, 'label')) {
      throw new TypeError('The label should be defined');
    }

    return direction;
  }
}
