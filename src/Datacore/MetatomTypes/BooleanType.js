import Metatom from '../Metatom';
import MetatomState from '../MetatomState';

 /**
  * MetatomType BooleanType
  *
  * @example
  * // with default configuration
  * const agree = new BooleanType('Agree?');
  * // { id: 'agreee', label: 'Agree?', required: true }
  *
  * // Custom ID
  * const agree = new BooleanType('Agree?', 'id_agree');
  * // { id: 'id_agreee', label: 'Agree?', required: true }
  *
  * // Config required = false;
  * const agree = new BooleanType('Agree?', {
  *   required: false
  * }, 'id_agree');
  * // { id: 'id_agreee', label: 'Agree?', required: false }
  *
  * @module Datacore
  * @class BooleanType
  */
export default class BooleanType extends Metatom {

  /**
   * @param  {String}  [label]
   * @param  {Object|String} [config|id]
   * @param  {String}  [id=null]
   * @return {BooleanType}
   */
  constructor(label, _config = {}, _id = null) {
    super(label, _config, _id);
    MetatomState(this).set('renderType', 'switch');
  }

  get className() {
    return 'BooleanType';
  }
}
