/* eslint no-restricted-syntax: ["off"] */
import Molecule from './Molecule';

const PUBLIC_KEY = '*';

export default class ACL {

  permissionsById = {};

  constructor(arg1) {
    this.permissionsById = {};
    if (arg1 && typeof arg1 === 'object') {
      if (arg1 instanceof Molecule && arg1.getMoleculoidName() === 'User') {
        this.setReadAccess(arg1, true);
        this.setWriteAccess(arg1, true);
      } else {
        Object.keys(arg1).forEach(userId => {
          const accessList = arg1[userId];
          if (typeof userId !== 'string') {
            throw new TypeError(
              'Tried to create an ACL with an invalid user id.',
            );
          }

          this.permissionsById[userId] = {};
          Object.keys(accessList).forEach(permission => {
            const allowed = accessList[permission];
            if (permission !== 'read' && permission !== 'write') {
              throw new TypeError(
                'Tried to create an ACL with an invalid permission type.',
              );
            }
            if (typeof allowed !== 'boolean') {
              throw new TypeError(
                'Tried to create an ACL with an invalid permission value.',
              );
            }
            this.permissionsById[userId][permission] = allowed;
          });
        });
      }
    } else if (typeof arg1 === 'function') {
      throw new TypeError(
        'ParseACL constructed with a function. Did you forget ()?',
      );
    }
  }

  toJSON() {
    return {
      ...this.permissionsById,
    };
  }

  equals(other) {
    if (!(other instanceof ACL)) {
      return false;
    }
    const users = Object.keys(this.permissionsById);
    const otherUsers = Object.keys(other.permissionsById);
    if (users.length !== otherUsers.length) {
      return false;
    }

    for (const u in this.permissionsById) {
      if (!other.permissionsById[u]) {
        return false;
      }
      if (this.permissionsById[u].read !== other.permissionsById[u].read) {
        return false;
      }
      if (this.permissionsById[u].write !== other.permissionsById[u].write) {
        return false;
      }
    }

    return true;
  }

  _setAccess(accessType, userId, allowed) {
    let newUserId = userId;
    if (newUserId instanceof Molecule) {
      if (newUserId.getMoleculoidName() === 'User') {
        newUserId = newUserId.id;
      } else if (newUserId.getMoleculoidName() === 'Role') {
        const name = newUserId.getAtoms('name');
        if (!name) {
          throw new TypeError('Role must have a name');
        }
        newUserId = `role:${name}`;
      }
    }

    if (typeof newUserId !== 'string') {
      throw new TypeError('userId must be a string.');
    }
    if (typeof allowed !== 'boolean') {
      throw new TypeError('allowed must be either true or false.');
    }
    let permissions = this.permissionsById[newUserId];
    if (!permissions) {
      if (!allowed) {
        // The user already doesn't have this permission, so no action is needed
        return;
      }
      permissions = {};
      this.permissionsById[newUserId] = permissions;
    }

    if (allowed) {
      this.permissionsById[newUserId][accessType] = true;
    } else {
      delete permissions[accessType];
      if (Object.keys(permissions).length === 0) {
        delete this.permissionsById[newUserId];
      }
    }
  }

  _getAccess(accessType, userId) {
    let newUserId = userId;

    if (newUserId instanceof Molecule) {
      if (newUserId.getMoleculoidName() === 'User') {
        newUserId = newUserId.id;
        if (!newUserId) {
          throw new Error('Cannot get access for a User without an ID');
        }
      } else if (newUserId.getMoleculoidName() === 'Role') {
        const name = newUserId.getName();
        if (!name) {
          throw new TypeError('Role must have a name');
        }
        newUserId = `role:${name}`;
      }
    }
    const permissions = this.permissionsById[newUserId];
    if (!permissions) {
      return false;
    }
    return !!permissions[accessType];
  }

  setReadAccess(userId, allowed) {
    this._setAccess('read', userId, allowed);
  }

  getReadAccess(userId) {
    return this._getAccess('read', userId);
  }

  setWriteAccess(userId, allowed) {
    this._setAccess('write', userId, allowed);
  }

  getWriteAccess(userId) {
    return this._getAccess('write', userId);
  }

  setPublicReadAccess(allowed) {
    this.setReadAccess(PUBLIC_KEY, allowed);
  }

  getPublicReadAccess() {
    return this.getReadAccess(PUBLIC_KEY);
  }

  setPublicWriteAccess(allowed) {
    this.setWriteAccess(PUBLIC_KEY, allowed);
  }

  getPublicWriteAccess() {
    return this.getWriteAccess(PUBLIC_KEY);
  }

  getRoleReadAccess(role) {
    let newRole = role;
    if (newRole instanceof Molecule && newRole.getMoleculoidName() === 'Role') {
      // Normalize to the String name
      newRole = newRole.getAtoms('name');
    }
    if (typeof newRole !== 'string') {
      throw new TypeError(
        'role must be a Molecule Role or a String',
      );
    }
    return this.getReadAccess(`role:${newRole}`);
  }

  getRoleWriteAccess(role) {
    let newRole = role;
    if (newRole instanceof Molecule && newRole.getMoleculoidName() === 'Role') {
      // Normalize to the String name
      newRole = newRole.getAtoms('name');
    }
    if (typeof newRole !== 'string') {
      throw new TypeError(
        'role must be a Molecule Role or a String',
      );
    }
    return this.getWriteAccess(`role:${newRole}`);
  }

  setRoleReadAccess(role, allowed) {
    let newRole = role;
    if (newRole instanceof Molecule && newRole.getMoleculoidName() === 'Role') {
      // Normalize to the String name
      newRole = newRole.getAtoms('name');
    }
    if (typeof newRole !== 'string') {
      throw new TypeError(
        'role must be a Molecule Role or a String',
      );
    }
    this.setReadAccess(`role:${newRole}`, allowed);
  }

  setRoleWriteAccess(role, allowed) {
    let newRole = role;
    if (newRole instanceof Molecule && newRole.getMoleculoidName() === 'Role') {
      // Normalize to the String name
      newRole = newRole.getAtoms('name');
    }
    if (typeof newRole !== 'string') {
      throw new TypeError(
        'role must be a Molecule Role or a String',
      );
    }
    this.setWriteAccess(`role:${newRole}`, allowed);
  }
}
