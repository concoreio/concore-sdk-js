import ACL from './ACL';

const AccessoryFactory = conf => {
  const { data } = conf;

  /**
   * AccessoryFactory provide commons accessors for classes
   */
  return class {

    constructor() {
      data.set(this, {});
    }

    /**
     * Return an Object Representation
     * @return {Object}
    */
    get attributes() {
      return data.get(this);
    }

    /**
     * Object id
     * @return {String}
     */
    get id() {
      return this.attributes.id;
    }

    /**
     * Object createdAt
     * @return {String}
     */
    get createdAt() {
      return this.attributes.createdAt;
    }

    /**
     * Object updatedAt
     * @return {String}
     */
    get updatedAt() {
      return this.attributes.updatedAt;
    }

    /**
     * Returns true if this object was created by the Datacore server
     * @return {Boolean}
     */
    existed() {
      return !!this.attributes.id;
    }

    /**
     * Alias for this.attributes
     * @return {Object}
     */
    getAttributes() {
      return this.attributes;
    }

    /**
     * Returns true if this object has never been saved to Datacore
     * @return {Boolean}
     */
    isNew() {
      return !this.attributes.id;
    }

    setACL(acl) {
      let newACL = acl;
      if (!(acl instanceof ACL)) {
        newACL = new ACL(acl);
      }

      const values = data.get(this);

      data.set(this, {
        ...values,
        ACL: newACL
      });

      return this;
    }

    getACL() {
      const values = data.get(this);
      return values.ACL || null;
    }
  };
};

/**
 * @ignore
 */
export default AccessoryFactory;
