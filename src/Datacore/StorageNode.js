let memMap = {};

export default class StorageController {

  getItem(key) {
    if ({}.hasOwnProperty.call(memMap, key)) {
      return memMap[key];
    }
    return null;
  }

  setItem(key, value) {
    memMap[key] = String(value);
  }

  removeItem(key) {
    delete memMap[key];
  }

  clear() {
    memMap = {};
  }
}
