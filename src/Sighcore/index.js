import Sighcore from './Sighcore';

import Actions from './Actions';

Object.assign(Sighcore, {
  Actions
});

export default Sighcore;
