const MockAdapter = require('axios-mock-adapter');
const axios = require('axios');

const mock = new MockAdapter(axios);

mock.onDelete(/\/molecule\/\w+/)
    .reply(200);

mock.onPost(/\/molecule\/\w+/)
  .reply(config => {
    let { data } = config;
    data = JSON.parse(data);

    if (!data.id) {
      return [200, {
        id: '92q7as3',
        createdAt: '2017-01-04T13:51:01.619Z',
        updatedAt: '2017-01-04T13:51:01.619Z',
        atoms: {
          ...data.atoms,
          salary: {
            ...data.atoms.salary,
            label: 'R$ 2.000,00'
          }
        }
      }];
    }

    return [200, {
      updatedAt: '2017-01-05T13:51:01.619Z'
    }];
  });

mock.onDelete(/\/moleculoid\/\w+/)
  .reply(200);

mock.onPost(/\/moleculoid\/\w+/)
  .reply(config => {
    let { data } = config;
    data = JSON.parse(data);

    if (!data.id) {
      return [200, {
        id: '92q7as3',
        createdAt: '2017-01-04T13:51:01.619Z',
        updatedAt: '2017-01-04T13:51:01.619Z'
      }];
    }

    return [200, {
      updatedAt: '2017-01-05T13:51:01.619Z'
    }];
  });

module.exports = rootParam => {
  const root = rootParam || global;
  root.expect = root.chai.expect;

  beforeEach(() => {
    // Using these globally-available Sinon features is preferrable, as they're
    // automatically restored for you in the subsequent `afterEach`
    root.sandbox = root.sinon.sandbox.create();
    root.stub = root.sandbox.stub.bind(root.sandbox);
    root.spy = root.sandbox.spy.bind(root.sandbox);
    root.mock = root.sandbox.mock.bind(root.sandbox);
    root.useFakeTimers = root.sandbox.useFakeTimers.bind(root.sandbox);
    root.useFakeXMLHttpRequest = root.sandbox.useFakeXMLHttpRequest.bind(root.sandbox);
    root.useFakeServer = root.sandbox.useFakeServer.bind(root.sandbox);
  });

  afterEach(() => {
    delete root.stub;
    delete root.spy;
    root.sandbox.restore();
  });
};
