global.chai.use(require('chai-as-promised'));

const mochaGlobals = require('./.globals.json').globals;

window.mocha.setup('bdd');
window.onload = () => {
  window.mocha.checkLeaks();
  window.mocha.globals(Object.keys(mochaGlobals));
  window.mocha.run();
  require('./setup')(window);
};
