import { isEmpty, slugify, toType } from '../../src/helpers';

describe('helpers', () => {
  describe('isEmpty', () => {
    it('undef, empty, null, object and array empty return true', () => {
      expect(isEmpty()).to.be.true;
      expect(isEmpty('')).to.be.true;
      expect(isEmpty(null)).to.be.true;
      expect(isEmpty({})).to.be.true;
      expect(isEmpty([])).to.be.true;
    });
    it('non empty return false', () => {
      expect(isEmpty('a')).to.be.false;
      expect(isEmpty(1)).to.be.false;
      expect(isEmpty({ x: 1 })).to.be.false;
      expect(isEmpty(['a'])).to.be.false;
    });
  });

  describe('slugify', () => {
    const slugs = {
      'John Doe': 'john_doe',
      'First Name': 'first_name',
      'Last-Name:àaãs*%ˆâs@12': 'last_nameass12'
    };

    Object.keys(slugs).forEach(name => {
      const slug = slugs[name];
      it(`${name} becomes ${slug}`, () => {
        expect(slugify(name)).to.be.equal(slug);
      });
    });
  });

  describe('toType', () => {
    const types = {
      string: 'Paulo',
      object: {},
      array: [],
      regexp: /[a]/,
      number: 1
    };

    Object.keys(types).forEach(type => {
      const item = types[type];
      it(`${item} is ${type}`, () => {
        expect(toType(item)).to.be.equal(type);
      });
    });
  });
});
