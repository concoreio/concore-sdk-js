import Concore from '../../src/Concore';

describe('Concore', () => {
  const url = 'http://localhost:1000';

  it('should be a singleton', () => {
    expect(Concore).to.equal(Concore);
  });

  it('should be a singleton', () => {
    expect(Concore).to.equal(Concore);
  });

  it('should have a init method', () => {
    spy(Concore, 'init');
  });

  it('should have a request method', () => {
    spy(Concore, 'request');
  });

  describe('init', () => {
    it('initialize twice changing params, but axios instance dont change', () => {
      Concore.init(url, 'appID', 'apiKey');
      const request = Concore.request();

      expect(Concore.request()).to.be.deep.equal(request);

      Concore.init('urlHttp', 'app', 'api');

      expect(Concore.request()).to.be.deep.equal(request);
    });
    it('initialize Concore before use', () => {
      expect(() => {
        Concore.axios = null;
        Concore.request();
      }).to.throw(Error);
    });
    it('non string serverURL throw an error', () => {
      expect(() => Concore.init()).to.throw(/serverURL must be a string/);
    });
    it('non string appID throw an error', () => {
      expect(() => Concore.init(url)).to.throw(/appID must be a string/);
    });
    it('non string apiKey throw an error', () => {
      expect(() => Concore.init(url, 'appID')).to.throw(/apiKey must be a string/);
    });
    it('axios instance', () => {
      expect(() => Concore.init(url, 'appID', 'apiKey')).to.not.throw(Error);
      expect(() => Concore.request()).to.not.throw(Error);
    });
  });
});
