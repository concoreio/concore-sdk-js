import Inflator from '../../../src/Datacore/MetatomTypeInflator';

const metatomTypes = {
  BooleanType: {
    id: 'bool',
    label: 'Boolean',
    order: 0,
    required: true,
    renderType: 'switch',
    metatomType: 'BooleanType'
  },
  Choice: {
    id: 'choice',
    label: 'Choice',
    order: 0,
    required: true,
    options: [{ value: 1, label: 2 }],
    multiple: false,
    renderType: 'select',
    metatomType: 'Choice'
  },
  Currency: {
    id: 'salary',
    label: 'Salary',
    order: 0,
    required: true,
    options: ['BRL'],
    currency: 'EUR',
    metatomType: 'Currency',
    renderType: 'currency'
  },
  DateTime: {
    id: 'Date',
    label: 'Date',
    order: 0,
    required: true,
    format: 'dd/mm/YYYY',
    type: 'date',
    pickers: {},
    renderType: 'datetime',
    metatomType: 'DateTime',
  },
  Duration: {
    id: 'Date',
    label: 'Date',
    order: 0,
    required: true,
    fields: ['days'],
    renderType: 'duration',
    metatomType: 'Duration',
  },
  Email: {
    id: 'Date',
    label: 'Date',
    order: 0,
    required: true,
    validation: null,
    renderType: 'email',
    metatomType: 'Email',
  },
  File: {
    id: 'file',
    label: 'File',
    order: 0,
    required: true,
    multiple: false,
    extensions: ['doc'],
    renderType: 'upload',
    metatomType: 'File',
  },
  Image: {
    id: 'image',
    label: 'Image',
    order: 0,
    required: true,
    multiple: false,
    extensions: ['png'],
    renderType: 'upload',
    metatomType: 'Image',
  },
  Json: {
    id: 'json',
    label: 'Json',
    order: 0,
    required: true,
    renderType: 'textarea',
    metatomType: 'Json'
  },
  List: {
    id: 'list',
    label: 'List',
    order: 0,
    required: true,
    renderType: 'list',
    metatomType: 'List'
  },
  Location: {
    id: 'location',
    label: 'Location',
    order: 0,
    required: true,
    renderType: 'location',
    metatomType: 'Location',
    validation: null
  },
  NumberType: {
    id: 'numb',
    label: 'Numero',
    order: 0,
    required: true,
    renderType: 'number',
    metatomType: 'NumberType'
  },
  Password: {
    id: 'passwd',
    label: 'Password',
    order: 0,
    required: true,
    renderType: 'password',
    metatomType: 'Password',
    algorithm: 'bcrypt',
    saltRounds: 0,
    validation: null
  },
  Reference: {
    id: 'ref',
    label: 'Rerefence',
    moleculoid: 'Wife',
    labelId: 'name',
    order: 0,
    required: true,
    multiple: true,
    renderType: 'reference',
    metatomType: 'Reference',
  },
  Text: {
    id: 'name',
    label: 'Name',
    order: 0,
    required: true,
    renderType: 'text',
    validation: null,
    metatomType: 'Text'
  },
  Url: {
    id: 'url',
    label: 'Url',
    order: 0,
    required: true,
    renderType: 'url',
    validation: null,
    metatomType: 'Url'
  }
};

describe('MetatomTypeInflator', () => {
  Object.keys(metatomTypes).forEach(metatom => {
    it(`Inflate ${metatom} and check attributes`, () => {
      const obj = metatomTypes[metatom];
      const inflated = Inflator.inflate(obj);
      expect(inflated.attributes).to.deep.equal(obj);
    });
  });
});
