import MoleculeQuery from '../../../src/Datacore/MoleculeQuery';

const defaultBody = {
  query: {
    bool: {
      must: {},
      must_not: {},
      filter: {},
    },
  },
  sort: [],
};

const suite = {
  singles: {
    ascending: {
      name: 'order by name asc',
      params: ['name'],
      check: 'sort',
      object: [{
        name: { order: 'asc' }
      }]
    },
    addAscending: {
      name: 'order by name asc',
      params: ['name'],
      check: 'sort',
      object: [{
        name: { order: 'asc' }
      }]
    },
    addDescending: {
      name: 'order by date desc',
      params: ['date'],
      check: 'sort',
      object: [{
        date: { order: 'desc' }
      }]
    },
    descending: {
      name: 'order by name desc',
      params: ['name'],
      check: 'sort',
      object: [{
        name: { order: 'desc' }
      }]
    },
    search: {
      name: 'search by query equals "name"',
      params: ['name'],
      check: 'query',
      object: {
        bool: {
          ...defaultBody.query.bool,
          must: {
            match: { _all: 'name' },
          },
        },
      },
    },
    contains: {
      name: 'name contains "Paulo" and "Debora"',
      params: ['name', ['Paulo', 'Debora']],
      check: 'query',
      object: {
        bool: {
          ...defaultBody.query.bool,
          filter: {
            match: { name: ['Paulo', 'Debora'] },
          },
        },
      },
    },
    equalTo: {
      name: 'name equalTo "Paulo"',
      params: ['name', 'Paulo'],
      check: 'query',
      object: {
        bool: {
          ...defaultBody.query.bool,
          filter: {
            term: { name: 'Paulo' },
          },
        },
      },
    },
    notEqualTo: {
      name: 'name notEqualTo "Paulo"',
      params: ['name', 'Paulo'],
      check: 'query',
      object: {
        bool: {
          ...defaultBody.query.bool,
          must_not: {
            term: { name: 'Paulo' },
          },
        },
      },
    },
    greaterThan: {
      name: 'age greater than 18',
      params: ['age', 18],
      check: 'query',
      object: {
        bool: {
          ...defaultBody.query.bool,
          filter: {
            range: {
              age: { gt: 18 },
            },
          },
        },
      }
    },
    greaterThanOrEqualTo: {
      name: 'age greater than or equal to 18',
      params: ['age', 18],
      check: 'query',
      object: {
        bool: {
          ...defaultBody.query.bool,
          filter: {
            range: {
              age: { gte: 18 },
            },
          },
        },
      }
    },
    lessThan: {
      name: 'age less than 18',
      params: ['age', 18],
      check: 'query',
      object: {
        bool: {
          ...defaultBody.query.bool,
          filter: {
            range: {
              age: { lt: 18 },
            },
          },
        },
      }
    },
    lessThanOrEqualTo: {
      name: 'age less than or equal to 18',
      params: ['age', 18],
      check: 'query',
      object: {
        bool: {
          ...defaultBody.query.bool,
          filter: {
            range: {
              age: { lte: 18 },
            },
          },
        },
      }
    },
    atoms: [{
      name: 'select just name and email atoms',
      params: ['name', 'email'],
      check: '_source',
      object: ['name', 'email'],
    }, {
      name: 'select just name and email atoms',
      params: [['name', 'email']],
      check: '_source',
      object: ['name', 'email'],
    }]
  },
  composed: [{
    methods: ['search', 'greaterThan', 'ascending'],
    body: {
      query: {
        bool: {
          ...defaultBody.query.bool,
          must: {
            match: { _all: 'name' },
          },
          filter: {
            range: {
              age: { gt: 18 },
            },
          },
        },
      },
      sort: [{
        name: { order: 'asc' }
      }]
    },
  }, {
    methods: ['equalTo', 'lessThan', 'descending'],
    body: {
      query: {
        bool: {
          ...defaultBody.query.bool,
          filter: {
            term: { name: 'Paulo' },
            range: {
              age: { lt: 18 },
            },
          },
        },
      },
      sort: [{
        name: { order: 'desc' }
      }]
    },
  }, {
    methods: ['notEqualTo', 'lessThanOrEqualTo', 'ascending', 'addDescending'],
    body: {
      query: {
        bool: {
          ...defaultBody.query.bool,
          must_not: {
            term: { name: 'Paulo' },
          },
          filter: {
            range: {
              age: { lte: 18 },
            },
          }
        },
      },
      sort: [{
        name: { order: 'asc' },
      }, {
        date: { order: 'desc' }
      }]
    },
  }],
};

describe('MoleculeQuery', () => {
  Object.keys(suite.singles).forEach(method => {
    describe(method, () => {
      let item = suite.singles[method];

      if (!Array.isArray(item)) {
        item = [item];
      }

      item.forEach(test => {
        const { check, name, params, object } = test;
        it(name, () => {
          const instance = new MoleculeQuery('Name');
          instance[method](...params);

          expect(instance.body[check]).to.be.deep.equal(object);
        });
      });
    });
  });

  suite.composed.forEach(compose => {
    it(compose.methods.join(', '), () => {
      const instance = new MoleculeQuery('Name');

      compose.methods.forEach(method => {
        const { params } = suite.singles[method];
        instance[method](...params);
      });

      expect(instance.body).to.be.deep.equal(compose.body);
    });
  });
});
