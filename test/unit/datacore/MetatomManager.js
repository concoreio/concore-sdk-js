// import MetatomManager from '../../../src/datacore/MetatomManager';
// import Text from '../../../src/datacore/MetatomTypes/Text';

// describe('MetatomManager', () => {
//   const context = {};
//   const key = 'list';

//   const nonMappedWeak = new WeakMap();

//   const obj = new Text('Name');
//   const anotherObj = new Text('Other');
//   const me = new Text('Me');

//   describe('constructor', () => {
//     it('throw and error needs context and context must be and Object', () => {
//       expect(() => new MetatomManager()).to.throw(/needs a context/);
//       expect(() => new MetatomManager([])).to.throw(/must be an Object/);
//     });

//     it('throw and error, needs a WeakMap', () => {
//       expect(() => new MetatomManager(context)).to.throw(/needs weakmap/);
//     });

//     it('throw and error, needs a key', () => {
//       expect(() => new MetatomManager(context, nonMappedWeak)).to.throw(/needs a key/);
//     });

//     it('throw a error weakMap not has mapped context', () => {
//       expect(() => new MetatomManager(context, nonMappedWeak, key)).to.throw(/has a mapping/);
//     });

//     it('not throw a error', () => {
//       const weakmap = new WeakMap();
//       weakmap.set(context, {});
//       expect(() => new MetatomManager(context, weakmap, key)).to.not.throw(Error);
//     });
//   });

//   describe('addItem', () => {
//     const weakmap = new WeakMap();
//     weakmap.set(context, {});
//     const mg = new MetatomManager(context, weakmap, key);

//     it('add one item then add another and check length is equal two', () => {
//       mg.addItem(obj);
//       expect(mg.collection).to.be.deep.equal([obj]);
//       mg.addItem(anotherObj);
//       expect(mg.collection.length).to.be.equal(2);
//     });

//     it('add non instance of Metatom throw an error', () => {
//       expect(() => mg.addItem('a')).to.throw(/must be an instance of Metatom/);
//     });
//   });

//   describe('removeItem', () => {
//     const weakmap = new WeakMap();
//     weakmap.set(context, {});
//     const mg = new MetatomManager(context, weakmap, key);
//     mg.addItem(obj);
//     mg.addItem(anotherObj);
//     mg.addItem(me);

//     it('remove one item and check this length', () => {
//       mg.removeItem(anotherObj);
//       expect(mg.collection).to.be.deep.equal([obj, me]);
//       expect(mg.collection.length).to.be.equal(2);
//     });

//     it('try remove unfinded object and check his length continues equal one', () => {
//       mg.removeItem({});
//       expect(mg.collection.length).to.be.equal(2);
//     });

//     it('try remove object by ID and check his length', () => {
//       mg.removeItem('me');
//       expect(mg.collection.length).to.be.equal(1);
//     });
//   });

//   describe('add', () => {
//     const weakmap = new WeakMap();
//     weakmap.set(context, {});
//     const mg = new MetatomManager(context, weakmap, key);

//     it('add two items and check length is equal two', () => {
//       mg.add([obj, anotherObj]);
//       expect(mg.collection).to.be.deep.equal([obj, anotherObj]);
//       expect(mg.collection.length).to.be.equal(2);
//     });

//     it('add one item and check length is equal three', () => {
//       mg.add([me]);
//       expect(mg.collection.length).to.be.equal(3);
//     });

//     it('add two items non instance of Metatom throw an error', () => {
//       expect(() => mg.add(['a', {}])).to.throw(/must be an instance of Metatom/);
//       expect(mg.collection.length).to.be.equal(3);
//     });

//     it('try add existed object throw an error', () => {
//       expect(() => mg.add([obj])).to.throw(/already exists/);
//     });
//   });

//   describe('remove', () => {
//     const weakmap = new WeakMap();
//     weakmap.set(context, {});
//     const mg = new MetatomManager(context, weakmap, key);

//     it('add two items and remove one and check deeply', () => {
//       mg.add([obj, anotherObj]);
//       mg.remove([anotherObj]);
//       expect(mg.collection).to.be.deep.equal([obj]);
//       expect(mg.collection.length).to.be.equal(1);
//     });

//     it('remove last one objec', () => {
//       mg.remove([obj]);
//       expect(mg.collection.length).to.be.equal(0);
//     });
//   });

//   describe('get', () => {
//     const weakmap = new WeakMap();
//     weakmap.set(context, {});
//     const mg = new MetatomManager(context, weakmap, key);

//     it('add two items and get all items', () => {
//       mg.add([obj, anotherObj]);
//       expect(mg.get()).to.be.deep.equal([obj, anotherObj]);
//       expect(mg.get().length).to.be.equal(2);
//     });

//     it('get the item by name', () => {
//       expect(mg.get('name')).to.be.deep.equal(obj);
//     });
//   });
// });
