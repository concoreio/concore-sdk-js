// import Metatom from '../../../src/datacore/Metatom';

// describe('Metatom', () => {
//   it('should be abstract class', () => {
//     expect(() => new Metatom()).to.throw(/abstract/);
//   });
// });

// class InputType extends Metatom {

//   static validateFunc(val) {
//     if (typeof val !== 'string') {
//       return 'funcValidField must be string';
//     }

//     if (val !== 'X') {
//       return 'funcValidField is invalid';
//     }

//     return false;
//   }

//   get schema() {
//     return {
//       stringField: {
//         type: 'string',
//         validate: /[A-B]/
//       },
//       numberField: 'number',
//       arrayField: 'array',
//       objectField: {
//         type: 'object',
//         require: false
//       },
//       regexpField: 'regexp',
//       funcValidField: InputType.validateFunc,
//       booleanField: {
//         type: 'boolean',
//         require: false,
//         default: true
//       },
//     };
//   }
// }

// const paramsGeneric = {
//   stringField: 'A',
//   numberField: 1,
//   arrayField: [1],
//   regexpField: /[a-x]/,
//   funcValidField: 'X'
// };

// const validations = {
//   stringField: {
//     tests: {
//       'is required': ['', null, undefined, {}, []],
//       'must be string': [{ a: 1 }, ['a'], 1, /[a-b]/],
//       'is invalid': ['c', 'ca', 'ab']
//     }
//   },
//   numberField: {
//     required: {
//       stringField: 'A'
//     },
//     tests: {
//       'is required': ['', null, undefined, {}, []],
//       'must be number': [{ a: 1 }, ['a'], 'a', /[a-b]/]
//     }
//   },
//   arrayField: {
//     required: {
//       stringField: 'A',
//       numberField: 1
//     },
//     tests: {
//       'is required': [null, undefined, [], {}, ''],
//       'must be array': [{ a: 1 }, 'a', 1, /[a-b]/]
//     }
//   },
//   objectField: {
//     required: {
//       stringField: 'A',
//       numberField: 1,
//       arrayField: [1]
//     },
//     tests: {
//       'must be object': [['a'], 'a', 1, /[a-b]/]
//     }
//   },
//   regexpField: {
//     required: {
//       stringField: 'A',
//       numberField: 1,
//       arrayField: [1]
//     },
//     tests: {
//       'is required': ['', null, undefined, {}, []],
//       'must be regexp': [{ a: 1 }, ['a'], 1, 'a'],
//     }
//   },
//   funcValidField: {
//     required: {
//       stringField: 'A',
//       numberField: 1,
//       arrayField: [1],
//       regexpField: /[a-b]/
//     },
//     tests: {
//       'must be string': [{ a: 1 }, ['a'], 1, /[a-b]/],
//       'is invalid': ['c', 'ca', 'ab']
//     }
//   },
//   booleanField: {
//     required: {
//       stringField: 'A',
//       numberField: 1,
//       arrayField: [1],
//       regexpField: /[a-b]/,
//       funcValidField: 'X'
//     },
//     tests: {
//       'must be boolean': [['a'], 'a', 1, /[a-b]/]
//     }
//   }
// };

// describe('InputType', () => {
//   it('throw error stringField is required', () => {
//     expect(() => new InputType()).to.throw('stringField is required');
//   });

//   Object.keys(validations).forEach(param => {
//     const validation = validations[param].tests;
//     let params = validations[param].required || {};
//     Object.keys(validation).forEach(error => {
//       const throwError = `${param} ${error}`;
//       const values = validation[error];
//       values.forEach(value => {
//         it(`${param} with value ${JSON.stringify(value)} throw error ${throwError}`, () => {
//           params = {
//             ...params,
//             [param]: value
//           };
//           expect(() => new InputType(params)).to.throw(throwError);
//         });
//       });
//     });
//   });

//   describe('property attributes', () => {
//     const mg = new InputType(paramsGeneric);
//     it('return correct attributes object', () => {
//       expect(mg.attributes).to.be.deep.equal({
//         ...paramsGeneric,
//         booleanField: true,
//         objectField: undefined
//       });
//     });

//     it('set new value and return correct attributes object', () => {
//       mg.set('stringField', 'B');
//       expect(mg.attributes).to.be.deep.equal({
//         ...paramsGeneric,
//         stringField: 'B',
//         booleanField: true,
//         objectField: undefined
//       });
//     });

//     it('cant be setted', () => {
//       expect(() => {
//         mg.attributes = {};
//       }).to.throw(Error);
//     });
//   });

//   describe('get method', () => {
//     const mg = new InputType(paramsGeneric);
//     it('return correct info', () => {
//       expect(mg.get('stringField')).to.be.equal(paramsGeneric.stringField);
//     });

//     it('return undefined for not found attribute', () => {
//       expect(mg.get('string')).to.be.equal(undefined);
//     });

//     it('return default value to booleanField', () => {
//       expect(mg.get('booleanField')).to.be.equal(true);
//     });

//     it('return correct object with default values', () => {
//       expect(mg.attributes).to.be.deep.equal({
//         ...paramsGeneric,
//         booleanField: true,
//         objectField: undefined
//       });
//     });
//   });

//   describe('set method', () => {
//     const mg = new InputType(paramsGeneric);
//     it('set invalid string throw exception invalid', () => {
//       expect(() => mg.set('stringField', 'C')).to.throw('stringField is invalid');
//     });

//     it('set empty array throw exception required', () => {
//       expect(() => mg.set('arrayField', [])).to.throw('arrayField is required');
//     });

//     it('set order return correct order', () => {
//       mg.set('order', 1);
//       expect(mg.get('order')).to.be.equal(1);
//     });

//     it('set non number order return throw', () => {
//       expect(() => mg.set('order', '1')).to.throw(/must be number/);
//     });

//     it('set new value and return correct value with get', () => {
//       mg.set('stringField', 'B');
//       expect(mg.get('stringField')).to.be.equal('B');
//     });
//   });

//   describe('has method', () => {
//     const mg = new InputType(paramsGeneric);
//     it('return false to undefined attribute', () => {
//       expect(mg.has('Field')).to.be.false;
//     });

//     it('return true to undefined attribute', () => {
//       expect(mg.has('stringField')).to.be.true;
//     });
//   });

//   describe('toJSON method', () => {
//     it('return correct object', () => {
//       const mg = new InputType(paramsGeneric);
//       expect(mg.toJSON()).to.be.deep.equal({
//         ...paramsGeneric,
//         booleanField: true,
//         objectField: undefined
//       });
//     });
//   });
// });
