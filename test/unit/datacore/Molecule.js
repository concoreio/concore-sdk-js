// import Molecule from '../../../src/Datacore/Molecule';

// describe('Molecule', () => {
//   const firstName = 'Paulo';
//   const lastName = 'Reis';
//   let extra = 'Nothing to Say';
//   let email = 'paulovitin@gmail.com';
//   const birth = new Date(1986, 5, 23);
//   const salary = { currency: 'BRL', value: 2000 };
//   const person = new Molecule('Person', {
//     firstName,
//     lastName,
//     extra,
//     email,
//     birth,
//     salary
//   });

//   describe('constructor', () => {
//     it('with invalid params throw an error', () => {
//       expect(() => new Molecule()).to.throw(/and is required/);
//       expect(() => new Molecule({})).to.throw(/must be a string/);
//       expect(() => new Molecule('')).to.throw(/and is required/);
//       expect(() => new Molecule('Person', [])).to.throw(/must be an object/);
//     });

//     it('not throw error with valid params', () => {
//       expect(() => new Molecule('Person', {})).to.not.throw(Error);
//     });
//   });

//   describe('properties', () => {
//     it('get id, createdAt, updatedAt, return undefined', () => {
//       expect(person.id).to.be.undefined;
//       expect(person.createdAt).to.be.undefined;
//       expect(person.updatedAt).to.be.undefined;
//     });
//   });

//   describe('get', () => {
//     it('atom and return the correct value', () => {
//       expect(person.get('firstName')).to.be.equal(firstName);
//       expect(person.get('birth').toString()).to.be.equal(birth.toString());
//       expect(person.get('salary')).to.be.equal(salary);
//       expect(person.get()).to.be.deep.equal({
//         salary,
//         firstName,
//         extra,
//         lastName,
//         email,
//         birth
//       });
//     });
//   });

//   describe('set', () => {
//     it('atom name and value, return the correct value', () => {
//       extra = 'No more';
//       person.set('extra', extra);
//       expect(person.get('extra')).to.be.equal(extra);
//     });

//     it('atom object, return the correct value', () => {
//       email = 'paulo@concore.io';
//       const newValue = 'newValue';
//       person.set({
//         email,
//         newValue
//       });

//       expect(person.get()).to.be.deep.equal({
//         salary,
//         firstName,
//         extra,
//         lastName,
//         email,
//         birth,
//         newValue
//       });
//     });
//   });

//   describe('remove', () => {
//     it('atom by name', () => {
//       person.remove('extra').remove('newValue');

//       expect(person.get()).to.be.deep.equal({
//         salary,
//         firstName,
//         lastName,
//         email,
//         birth
//       });

//       expect(person.get('exrra')).to.be.undefined;
//     });

//     it('atom id, createdAt, updatedAt throw error', () => {
//       expect(() => person.remove('id')).to.throw(/You cannot remove/);
//       expect(() => person.remove('createdAt')).to.throw(/You cannot remove/);
//       expect(() => person.remove('updatedAt')).to.throw(/You cannot remove/);
//     });
//   });

//   describe('existed', () => {
//     it('unsaved Molecule return false', () => {
//       expect(person.existed()).to.be.false;
//     });
//   });

//   describe('getAttributes', () => {
//     it('and return correct object', () => {
//       expect(person.getAttributes()).to.be.deep.equal({
//         moleculoid: 'Person',
//         atoms: {
//           salary,
//           firstName,
//           lastName,
//           email,
//           birth
//         }
//       });
//     });
//   });

//   describe('getMoleculoidName', () => {
//     it('and return Person', () => {
//       expect(person.getMoleculoidName()).to.be.equal('Person');
//     });
//   });

//   describe('isNew', () => {
//     it('unsaved return true', () => {
//       expect(person.isNew()).to.be.true;
//     });
//   });

//   describe('toJSON', () => {
//     const personIn = new Molecule('Person', {
//       firstName, lastName
//     });
//     it('return a correct JSON', () => {
//       expect(personIn.toJSON()).to.be.deep.equal({
//         moleculoid: 'Person',
//         atoms: {
//           firstName,
//           lastName
//         }
//       });
//     });
//   });

//   describe('destroy', () => {
//     it('unsaved return rejected promise', () => {
//       expect(person.destroy()).to.be.rejectedWith('The Molecule needs a ID to destroy.');
//     });

//     it('after save the Molecule', () => {
//       const personIn = new Molecule('Person', {
//         firstName,
//         lastName,
//         extra,
//         email,
//         birth,
//         salary
//       });

//       expect(personIn.save()).to.be.eventually.equal(personIn)
//         .then(personSaved => {
//           expect(personSaved.destroy()).to.be.eventually.equal(personIn)
//             .then(personDestroyed => {
//               expect(personDestroyed.id).to.be.undefined;
//               expect(personSaved.createdAt).to.be.undefined;
//               expect(personIn.updatedAt).to.be.undefined;
//               expect(personSaved.isNew()).to.be.true;
//               expect(personIn.existed()).to.be.false;
//             });
//         });
//     });
//   });

//   describe('save', () => {
//     it('Molecule and check if receive a id, createdAt, updatedAt and salary label', () => {
//       const personIn = new Molecule('Person', {
//         firstName,
//         lastName,
//         extra,
//         email,
//         birth,
//         salary
//       });

//       expect(personIn.save()).to.be.eventually.equal(personIn)
//         .then(personSaved => {
//           expect(personSaved.id).to.not.be.undefined;
//           expect(personSaved.createdAt).to.not.be.undefined;
//           expect(personSaved.updatedAt).to.not.be.undefined;
//           expect(personSaved.get('salary').label).to.be.equal('R$ 2.000,00');
//           expect(personSaved.isNew()).to.be.false;
//           expect(personIn.existed()).to.be.true;
//           expect(personSaved).to.be.deep.equal(personIn);
//         });
//     });
//   });
// });
