import Moleculoid from '../../../src/Datacore/Moleculoid';
// import Choice from '../../../src/Datacore/MetatomTypes/Choice';
import Text from '../../../src/Datacore/MetatomTypes/Text';

describe('Moleculoid', () => {
  // const choice = new Choice({
  //   label: 'Escolha',
  //   options: [{
  //     label: 'Opcao', value: 'opcao'
  //   }]
  // });
  const name = new Text('Name');
  const lastName = new Text('LastName');

  const person = new Moleculoid('Person', [
    name,
    lastName
  ]);

  describe('getTitle', () => {
    it('should return the first metatom', () => {
      expect(person.getTitle()).to.be.equal(name);
    });
  });

  // describe('constructor', () => {
  //   it('instance class without params', () => {
  //     expect(() => new Moleculoid()).to.throw(/cannot be empty/);
  //   });

  //   it('instance class without metatoms', () => {
  //     expect(() => new Moleculoid('Person')).to.throw(/must be an instance of Metatom/);
  //   });

  //   it('instance class with invalid metatoms', () => {
  //     expect(() => new Moleculoid('Person', [{}])).to.throw(/must be an instance of Metatom/);
  //   });

  //   it('instance class with valid metatoms but duplicate metatom', () => {
  //     expect(() => new Moleculoid('Person', [
  //       choice, name, choice
  //     ])).to.throw(/escolha already exists/);
  //   });

  //   it('instance class with valid metatoms and check slug is correct', () => {
  //     expect(person.getLabel()).to.be.equal('Person');
  //   });
  // });

  // describe('addMetatoms', () => {
  //   const personIn = new Moleculoid('Person', [
  //     choice, name
  //   ]);

  //   it('empty param return exception', () => {
  //     expect(() => personIn.addMetatoms()).to.throw(/must be an instance of Metatom/);
  //   });

  //   it('empty array do not return exception', () => {
  //     expect(() => personIn.addMetatoms([])).to.not.throw(/must be an instance of Metatom/);
  //   });
  //   it('array invalid items return exception', () => {
  //     expect(() => personIn.addMetatoms([{}])).to.throw(/must be an instance of Metatom/);
  //   });

  //   it('array with valid items do not return exception and check getMetatoms', () => {
  //     expect(() => personIn.addMetatoms([email])).to.not.throw(Error);

  //     expect(personIn.getMetatoms()).to.be.deep.equal([
  //       choice, name, email
  //     ]);
  //   });

  //   it('valid item do not return exception and check getMetatoms', () => {
  //     expect(() => personIn.addMetatoms(address)).to.not.throw(Error);

  //     expect(personIn.getMetatoms()).to.be.deep.equal([
  //       choice, name, email, address
  //     ]);
  //   });

  //   it('invalid item return exception and check getMetatoms', () => {
  //     expect(() => personIn.addMetatoms({})).to.throw(TypeError);

  //     expect(personIn.getMetatoms()).to.be.deep.equal([
  //       choice, name, email, address
  //     ]);
  //   });
  // });

  // describe('destroy', () => {
  //   it('unsaved Moleculoid return rejected promise', () => {
  //     expect(person.destroy()).to.be.rejectedWith('The Moleculoid needs a ID to destroy.');
  //   });

  //   it('after save the Moleculoid', () => {
  //     const personIn = new Moleculoid('Person', [
  //       choice, name
  //     ]);

  //     expect(personIn.save()).to.be.eventually.equal(personIn)
  //       .then(personSaved => {
  //         expect(personSaved.destroy()).to.be.eventually.equal(personIn)
  //           .then(personDestroyed => {
  //             expect(personDestroyed.id).to.be.undefined;
  //             expect(personSaved.createdAt).to.be.undefined;
  //             expect(personIn.updatedAt).to.be.undefined;
  //             expect(personSaved.isNew()).to.be.true;
  //             expect(personIn.existed()).to.be.false;
  //           });
  //       });
  //   });
  // });

  // describe('existed', () => {
  //   it('unsaved Moleculoid return false', () => {
  //     expect(person.existed()).to.be.false;
  //   });
  // });

  // describe('getAttributes', () => {
  //   it('and return correct object', () => {
  //     expect(personLabel.getAttributes()).to.be.deep.equal({
  //       name: 'Person',
  //       label: 'Pessoas',
  //       metatoms: [choice, name]
  //     });
  //   });
  // });

  // describe('getMetatoms', () => {
  //   it('and return correct array of metatoms', () => {
  //     expect(personLabel.getMetatoms()).to.be.deep.equal([
  //       choice, name
  //     ]);
  //   });

  //   it('with name return correct metatom', () => {
  //     expect(personLabel.getMetatoms('escolha')).to.be.deep.equal({ ...choice });
  //   });
  // });

  // describe('getName', () => {
  //   it('and return Person', () => {
  //     expect(person.getName()).to.be.equal('Person');
  //   });
  // });

  // describe('isNew', () => {
  //   it('unsaved Moleculoid return true', () => {
  //     expect(person.isNew()).to.be.true;
  //   });
  // });

  // describe('removeMetatoms', () => {
  //   const personIn = new Moleculoid('Person', [
  //     choice, name
  //   ]);

  //   it('remove item by name and check metatoms', () => {
  //     expect(() => personIn.removeMetatoms('escolha')).to.not.throw(Error);

  //     expect(personIn.getMetatoms()).to.be.deep.equal([
  //       name
  //     ]);
  //   });

  //   it('add items and remove array of items', () => {
  //     expect(() => personIn.addMetatoms([address, email])).to.not.throw(Error);

  //     expect(() => personIn.removeMetatoms([name, email])).to.not.throw(Error);

  //     expect(personIn.getMetatoms()).to.be.deep.equal([
  //       address
  //     ]);
  //   });
  // });

  // describe('save', () => {
  //   it('Moleculoid without metatoms, to be reject', () => {
  //     expect(() => person.removeMetatoms([name, choice])).to.not.throw(Error);
  //     expect(person.getMetatoms()).to.be.empty;
  //     expect(person.save()).to.be.rejectedWith('The Moleculoid required metatoms to save.');
  //   });

  //   it('Moleculoid and check if receive a id, createdAt and updatedAt', () => {
  //     const personIn = new Moleculoid('Person', [
  //       choice, name
  //     ]);

  //     expect(personIn.save()).to.be.eventually.equal(personIn)
  //       .then(personSaved => {
  //         expect(personSaved.id).to.not.be.undefined;
  //         expect(personSaved.createdAt).to.not.be.undefined;
  //         expect(personSaved.updatedAt).to.not.be.undefined;
  //         expect(personSaved.isNew()).to.be.false;
  //         expect(personIn.existed()).to.be.true;
  //         expect(personSaved).to.be.deep.equal(personIn);
  //       });
  //   });

  //   it('Moleculoid and send update check if date change', () => {
  //     const personIn = new Moleculoid('Person', [
  //       choice, name
  //     ]);

  //     expect(personIn.save()).to.be.eventually.equal(personIn)
  //       .then(personSaved => {
  //         const personClone = {
  //           id: personIn.id,
  //           createdAt: personIn.createdAt,
  //           updatedAt: personIn.updatedAt
  //         };

  //         // send update
  //         expect(personSaved.save()).to.be.eventually.equal(personIn)
  //           .then(() => {
  //             expect(personSaved.id).to.be.equal(personClone.id);
  //             expect(personSaved.createdAt).to.be.equal(personClone.createdAt);
  //             expect(personSaved.updatedAt).to.not.be.equal(personClone.updatedAt);
  //           });
  //       });
  //   });
  // });

  // describe('setName', () => {
  //   it('invalid and return throw', () => {
  //     expect(() => person.setName('Paulo Reis')).to.throw(/is invalid/);
  //     expect(() => person.setName({})).to.throw(/is invalid/);
  //     expect(person.getName()).to.be.equal('Person');
  //   });

  //   it('Person2 and check is correct', () => {
  //     const newName = 'Person2';
  //     expect(() => person.setName(newName)).to.not.throw(Error);
  //     expect(person.getName()).to.be.equal(newName);
  //   });
  // });

  // describe('setLabel', () => {
  //   it('invalid and return throw', () => {
  //     expect(() => personLabel.setLabel({})).to.throw(/be a string/);
  //     expect(personLabel.getLabel()).to.be.equal('Pessoas');
  //   });

  //   it('"Cadastro de Pessoas" and check is correct', () => {
  //     const label = 'Cadastro de Pessoas';
  //     expect(() => personLabel.setLabel(label)).to.not.throw(Error);
  //     expect(personLabel.getLabel()).to.be.equal(label);
  //   });
  // });

  // describe('toJSON', () => {
  //   const personIn = new Moleculoid('Person', [
  //     choice, name
  //   ]);
  //   it('return a correct JSON', () => {
  //     expect(personIn.toJSON()).to.be.deep.equal({
  //       name: 'Person',
  //       label: 'Person',
  //       metatoms: [choice.toJSON(), name.toJSON()]
  //     });
  //   });
  // });

  // describe('Query', () => {
  //   it('return instance of Query', () => {
  //     expect(Moleculoid.Query('Person')).to.be.instanceof(Query);
  //   });
  // });
});
