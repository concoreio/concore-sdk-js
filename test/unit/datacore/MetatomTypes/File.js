import File from '../../../../src/Datacore/MetatomTypes/File';

const params = {
  property: {
    label: {
      values: {
        allow: ['a', 'a_a', 'Test'],
        notallow: [null, [], {}]
      }
    },
    _config: {
      pre: 'test',
      values: {
        allow: [{}, { required: true, order: 1 }, { id: 'aaa' }],
        notallow: []
      }
    },
    extensions: {
      pre: 'test',
      values: {
        allow: [{ extensions: ['gif'] }, { extensions: ['jpg', 'png'] }],
        notallow: [{ extensions: [{}, null, []] }]
      }
    },
    multiple: {
      pre: 'test',
      values: {
        allow: [{ multiple: false }, { multiple: true }],
        notallow: [{ multiple: '' }, { multiple: null }, { multiple: [] }]
      }
    }
  },
  methods: {
    gets: {
      attributes: {
        data: {
          id: 'test',
          label: 'test',
          metatomType: 'File',
          order: 0,
          renderType: 'upload',
          extensions: ['jpg'],
          required: true,
          multiple: true
        },
        method: false
      },
      getId: {
        data: 'test',
      },
      getLabel: {
        data: 'test',
      },
      getMetatomType: {
        data: 'File',
      },
      getOrder: {
        data: 0,
      },
      getRequired: {
        data: true,
      },
      getExtensions: {
        data: ['jpg']
      },
      getMultiple: {
        data: true
      }
    },
    sets: {
      setLabel: {
        data: 'test2',
        back: 'getLabel',
      },
      setOrder: {
        data: 1,
        back: 'getOrder'
      },
      setRequired: {
        data: false,
        back: 'getRequired'
      },
      setExtensions: {
        data: ['jpg'],
        back: 'getExtensions'
      },
      setMultiple: {
        data: false,
        back: 'getMultiple'
      }
    }
  }
};

describe('File', () => {
  Object.keys(params.property).forEach(param => {
    const items = params.property[param].values;

    describe(`${param} with values allow`, () => {
      items.allow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new File(item)).to.not.throw(TypeError);
          } else {
            expect(() => new File(params.property[param].pre, item)).to.not.throw(TypeError);
          }
        });
      });
    });
    describe(`${param} with values not allow`, () => {
      items.notallow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new File(item)).to.throw(TypeError);
          } else {
            expect(() => new File(params.property[param].pre, item)).to.throw(TypeError);
          }
        });
      });
    });
  });

  const gets = params.methods.gets;
  describe('Methods gets of class', () => {
    Object.keys(gets).forEach(obj => {
      const object = gets[obj];
      const Class = new File('test', { extensions: ['jpg'], multiple: true });

      it(`${obj}() should return ${object.data}`, () => {
        if (typeof object.method === 'undefined') {
          expect(Class[obj]()).to.deep.equal(object.data);
        } else {
          expect(Class[obj]).to.deep.equal(object.data);
        }
      });
    });
  });

  const sets = params.methods.sets;
  describe('Methods sets of class', () => {
    Object.keys(sets).forEach(obj => {
      const object = sets[obj];
      const Class = new File('test', { extensions: ['jpg'], multiple: true });
      it(`${obj}() should return ${object.data}`, () => {
        Class[obj](object.data);
        expect(Class[object.back]()).to.deep.equal(object.data);
      });
    });
  });
});
