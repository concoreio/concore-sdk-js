import List from '../../../../src/Datacore/MetatomTypes/List';

describe('List', () => {
  describe('Constructor', () => {
    it('instanciar sem parâmetros deve retornar error', () => {
      expect(() => new List()).to.throw(/must be a string/);
    });
    it('label com tipo errado deve retornar error', () => {
      expect(() => new List({})).to.throw(/must be a string/);
    });
    it('configurar outro parâmetro deve retornar error', () => {
      const config = { options: ['test'] };
      expect(() => new List('Test', config)).to.throw(/dont have config/);
    });
  });

  describe('Methods of parents class', () => {
    const list = new List('Test');

    it('get() deve retornar um objeto de propriedades', () => {
      const obj = new List('Test');
      expect(list.attributes).to.deep.equal(obj.attributes);
    });
    it('getId() deve retornar o id definido', () => {
      expect(list.getId()).to.be.equal('test');
    });
    it('getLabe() deve retornar o label definido', () => {
      expect(list.getLabel()).to.be.equal('Test');
    });
    it('getMetatomType() deve retornar o metatomtype da classe', () => {
      expect(list.getMetatomType()).to.be.equal('List');
    });
    it('getOrder() deve retonar o order definido', () => {
      expect(list.getOrder()).to.be.equal(0);
    });
    it('getRequired() deve retornar o required definido', () => {
      expect(list.getRequired()).to.be.equal(true);
    });
    it('setLabel() com tipo errado deve retornar error', () => {
      expect(() => list.setLabel({})).to.throw(/must be a string/);
    });
    it('setLabel() deve ter sucesso', () => {
      list.setLabel('Test2');
      expect(list.getLabel()).to.be.equal('Test2');
    });
    it('setOrder() com tipo errado deve retornar error', () => {
      expect(() => list.setOrder({})).to.throw(/Order must be a number/);
    });
    it('setOrder() deve ter sucessor', () => {
      list.setOrder(1);
      expect(list.getOrder()).to.be.equal(1);
    });
    it('setRequired() com tipo errado deve retornar error', () => {
      expect(() => list.setRequired({})).to.throw(/must be a boolean/);
    });
    it('setRequired() deve ter sucesso', () => {
      list.setRequired(true);
      expect(list.getRequired()).to.be.equal(true);
    });
    it('toJSON() deve retonar um objeto de valores', () => {
      const params = {
        id: 'test',
        label: 'Test2',
        metatomType: 'List',
        order: 1,
        renderType: 'list',
        required: true
      };
      expect(list.toJSON()).to.deep.equal(params);
    });
    it('_setConfig() deve mudar multipas propriedades', () => {
      list._setConfig({ required: false, label: 'Test255' });
      const params = {
        id: 'test',
        label: 'Test255',
        metatomType: 'List',
        order: 1,
        renderType: 'list',
        required: false
      };

      expect(list.toJSON()).to.deep.equal(params);
    });
  });
});

