import DateTime from '../../../../src/Datacore/MetatomTypes/DateTime';

const params = {
  property: {
    label: {
      values: {
        allow: ['a', 'a_a', 'Test'],
        notallow: [null, [], {}]
      }
    },
    _config: {
      pre: 'test',
      values: {
        allow: [{}, { format: 'dd/mm/dd' }, { id: 'aaa' }],
        notallow: []
      }
    },
    pickers: {
      pre: 'test',
      values: {
        allow: [
          {
            pickers: {
              date: { datePickerMode: 'day', showWeeks: true }
            }
          },
          {
            pickers: {
              date: { datePickerMode: 'day', showWeeks: true },
              time: { hourStep: 1, minuteStep: 1 }
            }
          }
        ],
        notallow: []
      }
    }
  },
  methods: {
    gets: {
      attributes: {
        data: {
          id: 'test',
          label: 'test',
          metatomType: 'DateTime',
          order: 0,
          pickers: {},
          renderType: 'datetime',
          required: true,
          type: 'datetime',
          format: 'dd/mm/yyyy'
        },
        method: false
      },
      getId: {
        data: 'test',
        method: true
      },
      getLabel: {
        data: 'test',
        method: true
      },
      getMetatomType: {
        data: 'DateTime',
        method: true
      },
      getOrder: {
        data: 0,
        method: true
      },
      getRequired: {
        data: true,
        method: true
      },
      getFormat: {
        data: 'dd/mm/yyyy',
        method: true
      },
      getPickers: {
        data: {},
        method: true
      },
      getType: {
        data: 'datetime',
        method: true
      }
    },
    sets: {
      setLabel: {
        data: 'test2',
        back: 'getLabel',
      },
      setOrder: {
        data: 1,
        back: 'getOrder'
      },
      setRequired: {
        data: false,
        back: 'getRequired'
      },
      setFormat: {
        data: 'yyyy-mm-dd',
        back: 'getFormat'
      },
      setPickers: {
        data: { date: { showWeeks: true }, time: { hourStep: 1 } },
        back: 'getPickers'
      },
      setType: {
        data: 'time',
        back: 'getType'
      }
    }
  }
};

describe('DateTime', () => {
  Object.keys(params.property).forEach(param => {
    const items = params.property[param].values;

    describe(`${param} with values allow`, () => {
      items.allow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new DateTime(item)).to.not.throw(TypeError);
          } else {
            expect(() => new DateTime(params.property[param].pre, item)).to.not.throw(TypeError);
          }
        });
      });
    });
    describe(`${param} with values not allow`, () => {
      items.notallow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new DateTime(item)).to.throw(TypeError);
          } else {
            expect(() => new DateTime(params.property[param].pre, item)).to.throw(TypeError);
          }
        });
      });
    });
  });

  const gets = params.methods.gets;
  describe('Methods gets of class', () => {
    Object.keys(gets).forEach(obj => {
      const object = gets[obj];
      const Class = new DateTime('test', { format: 'dd/mm/yyyy' });
      it(`${obj}() should return ${object.data}`, () => {
        if (object.method) {
          expect(Class[obj]()).to.deep.equal(object.data);
        } else {
          expect(Class[obj]).to.deep.equal(object.data);
        }
      });
    });
  });

  const sets = params.methods.sets;
  describe('Methods sets of class', () => {
    Object.keys(sets).forEach(obj => {
      const object = sets[obj];
      const Class = new DateTime('test');
      it(`${obj}() should return ${object.data}`, () => {
        Class[obj](object.data);
        expect(Class[object.back]()).to.deep.equal(object.data);
      });
    });
  });
});
