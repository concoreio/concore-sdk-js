import Duration from '../../../../src/Datacore/MetatomTypes/Duration';

const params = {
  property: {
    label: {
      values: {
        allow: ['a', 'a_a', 'Test'],
        notallow: [null, [], {}]
      }
    },
    _config: {
      pre: 'test',
      values: {
        allow: [{}, { required: true, order: 1 }, { id: 'aaa' }],
        notallow: []
      }
    },
    fields: {
      pre: 'test',
      values: {
        allow: [{ fields: ['days', 'hours', 'minutes', 'seconds'] }],
        notallow: [{ fields: '' }, { fields: null }]
      }
    }
  },
  methods: {
    gets: {
      attributes: {
        data: {
          id: 'test',
          label: 'test',
          metatomType: 'Duration',
          order: 0,
          renderType: 'duration',
          required: true,
          fields: ['days', 'hours', 'minutes', 'seconds']
        },
        method: false
      },
      getId: {
        data: 'test',
        method: true
      },
      getLabel: {
        data: 'test',
        method: true
      },
      getMetatomType: {
        data: 'Duration',
        method: true
      },
      getOrder: {
        data: 0,
        method: true
      },
      getRequired: {
        data: true,
        method: true
      },
      getFields: {
        data: ['days', 'hours', 'minutes', 'seconds'],
        method: true
      }
    },
    sets: {
      setLabel: {
        data: 'test2',
        back: 'getLabel',
      },
      setOrder: {
        data: 1,
        back: 'getOrder'
      },
      setRequired: {
        data: false,
        back: 'getRequired'
      },
      setFields: {
        data: ['days', 'hours', 'minutes', 'seconds'],
        back: 'getFields'
      }
    }
  }
};

describe('Duration', () => {
  Object.keys(params.property).forEach(param => {
    const items = params.property[param].values;

    describe(`${param} with values allow`, () => {
      items.allow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new Duration(item)).to.not.throw(TypeError);
          } else {
            expect(() => new Duration(params.property[param].pre, item)).to.not.throw(TypeError);
          }
        });
      });
    });
    describe(`${param} with values not allow`, () => {
      items.notallow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new Duration(item)).to.throw(TypeError);
          } else {
            expect(() => new Duration(params.property[param].pre, item)).to.throw(TypeError);
          }
        });
      });
    });
  });

  const gets = params.methods.gets;
  describe('Methods gets of class', () => {
    Object.keys(gets).forEach(obj => {
      const object = gets[obj];
      const Class = new Duration('test', { fields: ['days', 'hours', 'minutes', 'seconds'] });
      it(`${obj}() should return ${object.data}`, () => {
        if (object.method) {
          expect(Class[obj]()).to.deep.equal(object.data);
        } else {
          expect(Class[obj]).to.deep.equal(object.data);
        }
      });
    });
  });

  const sets = params.methods.sets;
  describe('Methods sets of class', () => {
    Object.keys(sets).forEach(obj => {
      const object = sets[obj];
      const Class = new Duration('test', { fields: ['days', 'hours', 'minutes', 'seconds'] });
      it(`${obj}() should return ${object.data}`, () => {
        Class[obj](object.data);
        expect(Class[object.back]()).to.deep.equal(object.data);
      });
    });
  });
});
