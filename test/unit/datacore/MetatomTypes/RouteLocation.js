import RouteLocation from '../../../../src/Datacore/MetatomTypes/RouteLocation';

describe('RouteLocation', () => {
  describe('#getClassName', () => {
    it('should be a string', () => {
      expect(new RouteLocation('test').className).to.be.a('string');
    });
  });

  const route = new RouteLocation('test');
  const originError = { labelError: 'Origem' };
  const origin = { label: 'Origem' };

  describe('#getLanguage', () => {
    it('should be an string', () => {
      route.setLanguage('pt-BR');
      expect(route.getLanguage()).to.be.a('string');
    });
    it('should be the default language - en-US', () => {
      expect(new RouteLocation('test').getLanguage()).to.be.deep.equal('en-US');
    });
  });

  describe('#setLanguage', () => {
    it('should be an RouteLocation', () => {
      expect(route.setLanguage('pt-BR')).to.be.an.instanceof(RouteLocation);
    });
  });

  describe('#getMode', () => {
    it('should be an String', () => {
      expect(route.getMode()).to.be.a('string');
    });
    it('should be the default mode - Driving', () => {
      expect(new RouteLocation('test').getMode()).to.be.deep.equal('driving');
    });
  });

  describe('#setMode', () => {
    it('should be an RouteLocation', () => {
      expect(route.setMode('driving')).to.be.an.instanceof(RouteLocation);
    });
  });

  describe('#getOrigin', () => {
    it('should be an object', () => {
      expect(new RouteLocation('test').setOrigin(origin).getOrigin()).to.be.an('object');
    });
  });

  describe('#setOrigin', () => {
    it('should be an RouteLocation', () => {
      expect(new RouteLocation('test').setOrigin(origin)).to.be.an.instanceof(RouteLocation);
    });
    it('should throw an error', () => {
      expect(() => new RouteLocation('test')
        .setOrigin(originError))
        .to
        .throw(
          TypeError,
          /The label should be defined/
        );
    });
  });

  describe('#getDestination', () => {
    it('should be an object', () => {
      expect(new RouteLocation('test').setDestination(origin).getDestination()).to.be.an('object');
    });
  });

  describe('#setDestination', () => {
    it('should be an RouteLocation', () => {
      expect(new RouteLocation('test').setDestination(origin)).to.be.an.instanceof(RouteLocation);
    });
    it('should throw an error', () => {
      expect(() => new RouteLocation('test')
        .setDestination(originError))
        .to
        .throw(
          TypeError,
          /The label should be defined/
        );
    });
  });
});

