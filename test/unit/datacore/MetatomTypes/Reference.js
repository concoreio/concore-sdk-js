import References from '../../../../src/Datacore/MetatomTypes/Reference';
import Text from '../../../../src/Datacore/MetatomTypes/Text';
import Moleculoid from '../../../../src/Datacore/Moleculoid';

const params = {
  property: {
    label: {
      values: {
        allow: ['a', 'a_a', 'Test'],
        notallow: [null, [], {}]
      }
    },
    moleculoid: {
      pre: ['test'],
      values: {
        allow: ['aaa', 'adahda', 'x'],
        notallow: [{}, []]
      }
    }
  },
  methods: {
    gets: {
      attributes: {
        data: {
          id: 'test',
          label: 'test',
          metatomType: 'Moleculoid',
          order: 0,
          renderType: 'reference',
          required: true,
          moleculoid: 'aaabb'
        },
        method: false
      },
      getId: {
        data: 'test',
        method: true
      },
      getLabel: {
        data: 'test',
        method: true
      },
      getMetatomType: {
        data: 'Moleculoid',
        method: true
      },
      getOrder: {
        data: 0,
        method: true
      },
      getRequired: {
        data: true,
        method: true
      },
      getMoleculoid: {
        data: 'aaabb',
        method: true
      }
    },
    sets: {
      setLabel: {
        data: 'test2',
        back: 'getLabel',
      },
      setOrder: {
        data: 1,
        back: 'getOrder'
      },
      setRequired: {
        data: false,
        back: 'getRequired'
      },
      setMoleculoid: {
        data: 'bbbaa',
        back: 'getMoleculoid'
      }
    }
  }
};

describe('References', () => {
  Object.keys(params.property).forEach(param => {
    const items = params.property[param].values;

    describe(`${param} with values allow`, () => {
      items.allow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new References(item)).to.not.throw(TypeError);
          } else {
            const config = params.property[param].pre.join();
            expect(() => new References(config, item)).to.not.throw(TypeError);
          }
        });
      });
    });
    describe(`${param} with values not allow`, () => {
      items.notallow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new References(item)).to.throw(TypeError);
          } else {
            const config = params.property[param].pre.join();
            expect(() => new References(config, item)).to.throw(TypeError);
          }
        });
      });
    });
  });

  const gets = params.methods.gets;
  describe('Methods gets of class', () => {
    Object.keys(gets).forEach(obj => {
      const object = gets[obj];
      const Class = new References('test', 'aaabb');
      it(`${obj}() should return ${object.data}`, () => {
        if (object.method) {
          expect(Class[obj]()).to.deep.equal(object.data);
        } else {
          expect(Class[obj]).to.deep.equal(object.data);
        }
      });
    });
  });

  const sets = params.methods.sets;
  describe('Methods sets of class', () => {
    Object.keys(sets).forEach(obj => {
      const object = sets[obj];
      const Class = new References('test', 'bbbaa');
      it(`${obj}() should return ${object.data}`, () => {
        Class[obj](object.data);
        expect(Class[object.back]()).to.deep.equal(object.data);
      });
    });
  });

  describe('#getReference', () => {
    it('should return an moleculoid with self reference', () => {
      const jsonMoleculoid = new Moleculoid('test', [new References('test', 'test', { labelId: 'x' })]).toJSON();
      const moleculoid = Moleculoid._inflateMoleculoid(jsonMoleculoid);
      expect(moleculoid.getMetatoms('test').getReference()).to.be.an.instanceof(Moleculoid);
      expect(moleculoid.getMetatoms('test').getReference().getName()).to.deep.equal('test');
    });
    it('should return an moleculoid with multiples reference', () => {
      const jsonMoleculoidTestA = new Moleculoid('testA', [new Text('test')]).toJSON();
      const jsonMoleculoidTestB = new Moleculoid('testB', [
        new References('testB', 'testB', { labelId: 'x' }),
        new References('testA', 'testA', { labelId: 'x' })
      ])
      .toJSON();

      jsonMoleculoidTestB.references = { testA: jsonMoleculoidTestA };
      const moleculoid = Moleculoid._inflateMoleculoid(jsonMoleculoidTestB);
      expect(moleculoid.getMetatoms('testa').getReference()).to.be.an.instanceof(Moleculoid);
      expect(moleculoid.getMetatoms('testa').getReference().getName()).to.deep.equal('testA');
      expect(moleculoid.getMetatoms('testb').getReference().getName()).to.deep.equal('testB');
    });
  });
});
