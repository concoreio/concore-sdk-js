import Text from '../../../../src/Datacore/MetatomTypes/Text';

const params = {
  property: {
    label: {
      values: {
        allow: ['a', 'a_a', 'Test'],
        notallow: [null, [], {}]
      }
    },
    _config: {
      pre: 'test',
      values: {
        allow: [{}, { required: true, order: 1 }, { id: 'aaa' }],
        notallow: []
      }
    },
    rows: {
      pre: 'test',
      values: {
        allow: [{ rows: 3, renderType: 'textarea' }],
        notallow: [{ rows: [] }, { rows: {} }]
      }
    }
  },
  methods: {
    gets: {
      attributes: {
        data: {
          id: 'test',
          label: 'test',
          metatomType: 'Text',
          order: 0,
          renderType: 'text',
          required: true,
          validation: '/a-b/',
          rows: 1
        },
        method: false
      },
      getId: {
        data: 'test',
      },
      getLabel: {
        data: 'test',
      },
      getMetatomType: {
        data: 'Text',
      },
      getOrder: {
        data: 0,
      },
      getRequired: {
        data: true,
      },
      getValidation: {
        data: '/a-b/',
      },
      getRenderType: {
        data: 'text',
      },
      getRows: {
        data: 1,
      }
    },
    sets: {
      setLabel: {
        data: 'test2',
        back: 'getLabel',
      },
      setOrder: {
        data: 1,
        back: 'getOrder'
      },
      setRequired: {
        data: false,
        back: 'getRequired'
      },
      setValidation: {
        data: '/b-a/',
        back: 'getValidation'
      },
      setRenderType: {
        data: 'textarea',
        back: 'getRenderType'
      },
      setRows: {
        data: 3,
        back: 'getRows'
      }
    }
  }
};

describe('Text', () => {
  Object.keys(params.property).forEach(param => {
    const items = params.property[param].values;

    describe(`${param} with values allow`, () => {
      items.allow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new Text(item)).to.not.throw(TypeError);
          } else {
            expect(() => new Text(params.property[param].pre, item)).to.not.throw(TypeError);
          }
        });
      });
    });
    describe(`${param} with values not allow`, () => {
      items.notallow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new Text(item)).to.throw(TypeError);
          } else {
            expect(() => new Text(params.property[param].pre, item)).to.throw(TypeError);
          }
        });
      });
    });
  });

  const gets = params.methods.gets;
  describe('Methods gets of class', () => {
    Object.keys(gets).forEach(obj => {
      const object = gets[obj];
      const Class = new Text('test', { validation: '/a-b/', rows: 1 });
      it(`${obj}() should return ${object.data}`, () => {
        if (typeof object.method === 'undefined') {
          expect(Class[obj]()).to.deep.equal(object.data);
        } else {
          expect(Class[obj]).to.deep.equal(object.data);
        }
      });
    });
  });

  const sets = params.methods.sets;
  describe('Methods sets of class', () => {
    Object.keys(sets).forEach(obj => {
      const object = sets[obj];
      const Class = new Text('test', { validation: '/a-b/' });
      it(`${obj}() should return ${object.data}`, () => {
        Class[obj](object.data);
        expect(Class[object.back]()).to.deep.equal(object.data);
      });
    });
  });
});
