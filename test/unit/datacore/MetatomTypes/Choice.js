import Choice from '../../../../src/Datacore/MetatomTypes/Choice';

describe('Choice', () => {
  const options = [
    { label: 'Bahia', value: 'BA' },
    { label: 'São Paulo', value: 'SP' }
  ];

  describe('Constructor', () => {
    it('instanciar sem parametros deve retornar error', () => {
      expect(() => new Choice()).to.throw(/must be a string/);
    });
    it('label com tipo errado deve retornar error', () => {
      expect(() => new Choice({})).to.throw(/must be a string/);
    });
    it('options com tipo errado deve retornar error', () => {
      expect(() => new Choice('Test', '')).to.throw(/must be an array/);
    });
    it('options vazio deve retornar error', () => {
      expect(() => new Choice('Test', [])).to.throw(/cant be empty/);
    });
    it('options com item vazio deve retornar error', () => {
      expect(() => new Choice('Test', [{}])).to.throw(/with value and label/);
    });
    it('item de options sem label deve retornar error', () => {
      const opt = [{ value: 'M' }];
      expect(() => new Choice('Test', opt)).to.throw(/with value and label/);
    });
    it('item de options sem value deve retornar error', () => {
      const opt = [{ label: 'Fem' }];
      expect(() => new Choice('Test', opt)).to.throw(/with value and label/);
    });
    it('required com tipo errado deve retornar error', () => {
      const config = { required: [] };
      expect(() => new Choice('Test', options, config)).to.throw(/must be a boolean/);
    });
    it('order com tipo errado deve retornar error', () => {
      const config = { order: null };
      expect(() => new Choice('Test', options, config)).to.throw(/must be a number/);
    });
    it('renderType com tipo errado deve retornar error', () => {
      const config = { renderType: null };
      expect(() => new Choice('Test', options, config)).to.throw(/type not allowed/);
    });
    it('renderType com valor errado deve retornar error', () => {
      const config = { renderType: 'text' };
      expect(() => new Choice('Test', options, config)).to.throw(/checkbox, radio, select/);
    });
    it('multipe com tipo errado deve retornar error', () => {
      const config = { multiple: [] };
      expect(() => new Choice('Test', options, config)).to.throw(/must be a boolean/);
    });

    it('id com tipo errado deve retornar error', () => {
      expect(() => new Choice('Test', options, {}, [])).to.throw(/is invalid/);
    });
    it('addOptions() deve adicionar um novo item', () => {
      const option = { label: 'Brasil', value: 'BR' };
      const choice = new Choice('Test', options);
      choice.addOption(option);
      expect(choice.getOptions()).to.deep.equal([
        { label: 'Bahia', value: 'BA' },
        { label: 'São Paulo', value: 'SP' },
        { label: 'Brasil', value: 'BR' }
      ]);
    });
    it('removeOption() deve remover um item', () => {
      const obj = new Choice('Test', options);
      obj.removeOption({ label: 'Bahia', value: 'BA' });
      expect(obj.getOptions()).to.deep.equal([
        { label: 'São Paulo', value: 'SP' },
        { label: 'Brasil', value: 'BR' }
      ]);
    });
    it('removeOptions deve remover multiplos items', () => {
      const obj1 = new Choice('Test', options);
      obj1.removeOptions([
        { label: 'São Paulo', value: 'SP' },
        { label: 'Brasil', value: 'BR' }
      ]);
      expect(obj1.getOptions()).to.deep.equal([
        { label: 'Bahia', value: 'BA' },
      ]);
    });
    it('setMultiple() deve modificar o multipe', () => {
      const choice = new Choice('Test', options);
      choice.setMultiple(true);
      expect(choice.getMultiple()).to.be.equal(true);
    });
    it('setOPtions() deve setar array de objetos', () => {
      const choice = new Choice('Test', options);
      choice.setOptions([{ label: 'test', value: 't' }]);
      expect(choice.getOptions()).to.deep.equal([
        { label: 'test', value: 't' }
      ]);
    });
    it('setRenderType() deve setar o renderType', () => {
      const choice = new Choice('Test', options, { renderType: 'radio' });
      choice.setRenderType('checkbox');
      expect(choice.getRenderType()).to.be.equal('checkbox');
    });
  });

  describe('Methods parent of class', () => {
    const json = {
      label: 'Test',
      options: [
        { label: 'Estados Unidos', value: 'EUA' }
      ],
      config: {
        required: false,
        order: 0,
        multiple: false
      },
      metatomType: 'Choice',
      id: 'test',
      renderType: 'select'
    };

    const attrs = {
      label: 'Test',
      options: [
        { label: 'Estados Unidos', value: 'EUA' }
      ],
      required: false,
      order: 0,
      multiple: false,
      metatomType: 'Choice',
      id: 'test',
      renderType: 'select'
    };

    const choice = new Choice('Test', json.options, json.config, 'test');
    it('attributes deve retornar um objeto de valores', () => {
      expect(choice.attributes).to.deep.equal(attrs);
    });
    it('getI() deve retornar o id', () => {
      expect(choice.getId()).to.be.equal('test');
    });
    it('getLabel() deve retornar o label', () => {
      expect(choice.getLabel()).to.be.equal('Test');
    });
    it('getMetatomType() deve retornar o metatomType', () => {
      expect(choice.getMetatomType()).to.be.equal('Choice');
    });
    it('getOrder() deve retornar o o order', () => {
      expect(choice.getOrder()).to.be.equal(0);
    });
    it('getRequired() deve retornar o required', () => {
      expect(choice.getRequired()).to.be.equal(false);
    });
    it('setLabel() deve mudar o label', () => {
      choice.setLabel('Test2');
      expect(choice.getLabel()).to.be.equal('Test2');
    });
    it('setLabel() com tipo errado deve retornar error', () => {
      expect(() => choice.setLabel([])).to.throw(/must be a string./);
    });
    it('setOrder() deve mudar o order', () => {
      choice.setOrder(1);
      expect(choice.getOrder()).to.be.equal(1);
    });
    it('setRequired() deve mudar o required', () => {
      choice.setRequired(true);
      expect(choice.getRequired()).to.be.equal(true);
    });
    it('toJSON() deve retornar um objeto de valores', () => {
      attrs.order = 1;
      attrs.required = true;
      attrs.label = 'Test2';
      expect(choice.toJSON()).to.deep.equal(attrs);
    });
    it('_setConfig() deve mudar multiplos parametros', () => {
      const config = { label: 'test33', order: 33 };
      choice._setConfig(config);
      attrs.label = 'test33';
      attrs.order = 33;
      expect(choice.attributes).to.deep.equal(attrs);
    });
    it('_setConfig() com parametros errados deve retornar error', () => {
      const config = { label: ['test21'] };
      expect(() => choice._setConfig(config)).to.throw(/must be a string/);
    });
  });
});
