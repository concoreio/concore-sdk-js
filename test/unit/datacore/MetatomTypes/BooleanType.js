import BooleanType from '../../../../src/Datacore/MetatomTypes/BooleanType';

describe('BooleanType', () => {
  describe('Constructor', () => {
    it('instanciar sem parâmetros deve retornar error', () => {
      expect(() => new BooleanType()).to.throw(/must be a string/);
    });
    it('label com tipo errado deve retornar error', () => {
      expect(() => new BooleanType({})).to.throw(/must be a string/);
    });
    it('configurar renderType deve retornar error', () => {
      const config = { renderType: 'switch' };
      expect(() => new BooleanType('Test', config)).to.throw(/dont have config/);
    });
    it('configurar outro parâmetro deve retornar error', () => {
      const config = { options: ['test'] };
      expect(() => new BooleanType('Test', config)).to.throw(/dont have config/);
    });
  });

  describe('Methods of parents class', () => {
    const bool = new BooleanType('Test');
    const json = {
      id: 'test',
      label: 'Test2',
      metatomType: 'BooleanType',
      order: 1,
      renderType: 'switch',
      required: true
    };

    it('get() deve retornar um objeto de propriedades', () => {
      const obj = new BooleanType('Test');
      expect(bool.attributes).to.deep.equal(obj.attributes);
    });
    it('getId() deve retornar o id definido', () => {
      expect(bool.getId()).to.be.equal('test');
    });
    it('getLabe() deve retornar o label definido', () => {
      expect(bool.getLabel()).to.be.equal('Test');
    });
    it('getMetatomType() deve retornar o metatomtype da classe', () => {
      expect(bool.getMetatomType()).to.be.equal('BooleanType');
    });
    it('getOrder() deve retonar o order definido', () => {
      expect(bool.getOrder()).to.be.equal(0);
    });
    it('getRequired() deve retornar o required definido', () => {
      expect(bool.getRequired()).to.be.equal(true);
    });
    it('setLabel() com tipo errado deve retornar error', () => {
      expect(() => bool.setLabel({})).to.throw(/must be a string/);
    });
    it('setLabel() deve ter sucesso', () => {
      bool.setLabel('Test2');
      expect(bool.getLabel()).to.be.string;
    });
    it('setOrder() com tipo errado deve retornar error', () => {
      expect(() => bool.setOrder({})).to.throw(/Order must be a number/);
    });
    it('setOrder() deve ter sucessor', () => {
      bool.setOrder(1);
      expect(bool.getOrder()).to.be.equal(1);
    });
    it('setRequired() com tipo errado deve retornar error', () => {
      expect(() => bool.setRequired({})).to.throw(/must be a boolean/);
    });
    it('setRequired() deve ter sucesso', () => {
      bool.setRequired(true);
      expect(bool.getRequired()).to.be.equal(true);
    });
    it('setRenderType() deve retornar error', () => {
      expect(() => bool.setRenderType('choice')).to.throw(/is not a function/);
    });
    it('toJSON() deve retonar um objeto de valores', () => {
      expect(bool.toJSON()).to.deep.equal(json);
    });
    it('_setConfig() pra mudar renderType deve retornar error', () => {
      const config = { renderType: 'choice' };
      expect(() => bool._setConfig(config)).to.throw(/dont have config/);
    });
    it('_setConfig() deve mudar multipas propriedades', () => {
      bool._setConfig({ required: false, label: 'Test255' });
      json.required = false;
      json.label = 'Test255';

      expect(bool.toJSON()).to.deep.equal(json);
    });
  });
});
