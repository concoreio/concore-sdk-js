import Password from '../../../../src/Datacore/MetatomTypes/Password';

const params = {
  property: {
    label: {
      values: {
        allow: ['a', 'a_a', 'Test'],
        notallow: [null, [], {}]
      }
    },
    _config: {
      pre: 'test',
      values: {
        allow: [{}, { required: true, order: 1 }, { id: 'aaa' }],
        notallow: []
      }
    },
    algorithm: {
      pre: 'ajaja',
      values: {
        allow: [{ algorithm: 'textPlain' }, { algorithm: 'md5' }, { algorithm: 'bcrypt' }],
        notallow: [{ algorithm: 'hash78' }, { algorithm: 'text' }, { algorithm: 'bit8' }],
      }
    }
  },
  methods: {
    gets: {
      attributes: {
        data: {
          id: 'test',
          label: 'test',
          metatomType: 'Password',
          order: 0,
          renderType: 'password',
          required: true,
          algorithm: 'md5',
          saltRounds: 0
        },
        method: false
      },
      getId: {
        data: 'test',
        method: true
      },
      getLabel: {
        data: 'test',
        method: true
      },
      getMetatomType: {
        data: 'Password',
        method: true
      },
      getOrder: {
        data: 0,
        method: true
      },
      getRequired: {
        data: true,
        method: true
      },
      getAlgorithm: {
        data: 'md5',
        method: true
      },
      getSaltRounds: {
        data: 0,
        method: true
      }
    },
    sets: {
      setLabel: {
        data: 'test2',
        back: 'getLabel',
      },
      setOrder: {
        data: 1,
        back: 'getOrder'
      },
      setRequired: {
        data: false,
        back: 'getRequired'
      },
      setAlgorithm: {
        data: 'bcrypt',
        back: 'getAlgorithm'
      },
      setSaltRounds: {
        data: 12,
        back: 'getSaltRounds'
      }
    }
  }
};

describe('Password', () => {
  Object.keys(params.property).forEach(param => {
    const items = params.property[param].values;

    describe(`${param} with values allow`, () => {
      items.allow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new Password(item, { algorithm: 'bcrypt', saltRouds: 10 })).to.not.throw(TypeError);
          } else {
            expect(() => new Password(params.property[param].pre, item)).to.not.throw(TypeError);
          }
        });
      });
    });
    describe(`${param} with values not allow`, () => {
      items.notallow.forEach(item => {
        it(`with ${item}`, () => {
          if (typeof params.property[param].pre === 'undefined') {
            expect(() => new Password(item, { algorithm: 'bcrypt', saltRouds: 10 })).to.throw(TypeError);
          } else {
            expect(() => new Password(params.property[param].pre, item, { algorithm: 'balgorithm', saltRouds: 10 })).to.throw(TypeError);
          }
        });
      });
    });
  });

  const gets = params.methods.gets;
  describe('Methods gets of class', () => {
    Object.keys(gets).forEach(obj => {
      const object = gets[obj];
      const Class = new Password('test');
      it(`${obj}() should return ${object.data}`, () => {
        if (object.method) {
          expect(Class[obj]()).to.deep.equal(object.data);
        } else {
          expect(Class[obj]).to.deep.equal(object.data);
        }
      });
    });
  });

  const sets = params.methods.sets;
  describe('Methods sets of class', () => {
    Object.keys(sets).forEach(obj => {
      const object = sets[obj];
      const Class = new Password('test');
      it(`${obj}() should return ${object.data}`, () => {
        Class[obj](object.data);
        expect(Class[object.back]()).to.deep.equal(object.data);
      });
    });
  });
});
