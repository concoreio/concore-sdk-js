import Email from '../../../../src/Datacore/MetatomTypes/Email';

describe('Email', () => {
  describe('Constructor', () => {
    it('instanciar sem parâmetros deve retornar error', () => {
      expect(() => new Email()).to.throw(/must be a string/);
    });
    it('label com tipo errado deve retornar error', () => {
      expect(() => new Email({})).to.throw(/must be a string/);
    });
    it('configurar outro parâmetro deve retornar error', () => {
      const config = { options: ['test'] };
      expect(() => new Email('Test', config)).to.throw(/dont have config/);
    });
  });

  describe('Methods of parents class', () => {
    const email = new Email('Test');
    const json = {
      id: 'test',
      label: 'Test2',
      metatomType: 'Email',
      order: 1,
      renderType: 'email',
      required: true,
      validation: null
    };

    it('get() deve retornar um objeto de propriedades', () => {
      const obj = new Email('Test');
      expect(email.attributes).to.deep.equal(obj.attributes);
    });
    it('getId() deve retornar o id definido', () => {
      expect(email.getId()).to.be.equal('test');
    });
    it('getLabe() deve retornar o label definido', () => {
      expect(email.getLabel()).to.be.equal('Test');
    });
    it('getMetatomType() deve retornar o metatomtype da classe', () => {
      expect(email.getMetatomType()).to.be.equal('Email');
    });
    it('getOrder() deve retonar o order definido', () => {
      expect(email.getOrder()).to.be.equal(0);
    });
    it('getRequired() deve retornar o required definido', () => {
      expect(email.getRequired()).to.be.equal(true);
    });
    it('setLabel() com tipo errado deve retornar error', () => {
      expect(() => email.setLabel({})).to.throw(/must be a string/);
    });
    it('setLabel() deve ter sucesso', () => {
      email.setLabel('Test2');
      expect(email.getLabel()).to.be.equal('Test2');
    });
    it('setOrder() com tipo errado deve retornar error', () => {
      expect(() => email.setOrder({})).to.throw(/Order must be a number/);
    });
    it('setOrder() deve ter sucessor', () => {
      email.setOrder(1);
      expect(email.getOrder()).to.be.equal(1);
    });
    it('setRequired() com tipo errado deve retornar error', () => {
      expect(() => email.setRequired({})).to.throw(/must be a boolean/);
    });
    it('setRequired() deve ter sucesso', () => {
      email.setRequired(true);
      expect(email.getRequired()).to.be.equal(true);
    });
    it('toJSON() deve retonar um objeto de valores', () => {
      expect(email.toJSON()).to.deep.equal(json);
    });
    it('_setConfig() deve mudar multipas propriedades', () => {
      email._setConfig({ required: false, label: 'Test255' });
      json.required = false;
      json.label = 'Test255';

      expect(email.toJSON()).to.deep.equal(json);
    });
  });
});

