import Currency from '../../../../src/Datacore/MetatomTypes/Currency';

describe('Currency', () => {
  describe('Constructor', () => {
    it('instanciar sem parâmetros deve retornar error', () => {
      expect(() => new Currency()).to.throw(/must be a string/);
    });
    it('label com tipo errado deve retornar error', () => {
      expect(() => new Currency({})).to.throw(/must be a string/);
    });
    it('currency com tipo errado deve retornar error', () => {
      const config = { currency: {} };
      expect(() => new Currency('Test', config)).to.throw(/must be a string/);
    });
    it('configurar renderType deve retornar error', () => {
      const config = { renderType: 'switch' };
      expect(() => new Currency('Test', config)).to.throw(/dont have config/);
    });
    it('configurar outro parâmetro deve retornar error', () => {
      const config = { options: {} };
      expect(() => new Currency('Test', config)).to.throw(/must be an array/);
    });
    it('addOption() com option errado deve retornar error', () => {
      const currency = new Currency('Test');
      expect(() => currency.addOption({})).to.throw(/must be an array/);
    });
    it('addOptions() com options inválidas deve retornar error', () => {
      const currency = new Currency('Test');
      const opts = { item1: null, item2: null };
      expect(() => currency.addOptions(opts)).to.throw(/must be an array/);
    });
    it('getCurrency() deve retornar o currency', () => {
      const currency = new Currency('Test');
      expect(currency.getCurrency()).to.be.equal('USD');
    });
    it('getOptions() deve retornar array de options', () => {
      const config = {
        options: ['BRL', 'LBL']
      };
      const currency = new Currency('Test', config);
      expect(currency.getOptions()).to.deep.equal(['BRL', 'LBL']);
    });
    it('removeOption() deve remover um item', () => {
      const config = {
        options: ['BRL', 'LBL']
      };
      const currency = new Currency('Test', config);
      currency.removeOption('LBL');
      expect(currency.getOptions()).to.deep.equal(['BRL']);
    });
    it('removeOptions() deve remover multiplos item', () => {
      const config = {
        options: ['BRL', 'LBL', 'USD']
      };
      const currency = new Currency('Test', config);
      currency.removeOptions(['LBL', 'USD']);
      expect(currency.getOptions()).to.deep.equal(['BRL']);
    });
    it('setOptions() deve substituir novos items', () => {
      const config = {
        options: ['BLR']
      };
      const currency = new Currency('Test', config);
      currency.setOptions(['USD', 'LBL']);
      expect(currency.getOptions()).to.deep.equal(['USD', 'LBL']);
    });
  });

  describe('Methods of parent class', () => {
    const config = {
      options: ['BRL'],
      required: true,
      order: 2
    };
    const currency = new Currency('Test', config);
    it('attributes deve retornar todos os atributos', () => {
      expect(currency.attributes).to.deep.equal({
        label: 'Test',
        id: 'test',
        metatomType: 'Currency',
        order: 2,
        currency: 'USD',
        options: ['BRL'],
        required: true,
        renderType: 'currency'
      });
    });
    it('getId() deve retonar o id', () => {
      expect(currency.getId()).to.be.equal('test');
    });
    it('getLabel() deve retonar o label', () => {
      expect(currency.getLabel()).to.be.equal('Test');
    });
    it('getMetatomType() deve retornar o metatomType', () => {
      expect(currency.getMetatomType()).to.be.equal('Currency');
    });
    it('getOrder() deve retornar o order', () => {
      expect(currency.getOrder()).to.be.equal(2);
    });
    it('getRequired() deve retornar o required', () => {
      expect(currency.getRequired()).to.be.equal(true);
    });
    it('setLabel() com tipo errado deve retornar error', () => {
      expect(() => currency.setLabel({})).to.throw(/must be a string/);
    });
    it('setLabel() deve ter sucesso', () => {
      currency.setLabel('Test2');
      expect(currency.getLabel()).to.be.equal('Test2');
    });
    it('setOrder() com tipo errado deve retornar error', () => {
      expect(() => currency.setOrder({})).to.throw(/Order must be a number/);
    });
    it('setOrder() deve ter sucessor', () => {
      currency.setOrder(1);
      expect(currency.getOrder()).to.be.equal(1);
    });
    it('setRequired() com tipo errado deve retornar error', () => {
      expect(() => currency.setRequired({})).to.throw(/must be a boolean/);
    });
    it('setRequired() deve ter sucesso', () => {
      currency.setRequired(true);
      expect(currency.getRequired()).to.be.equal(true);
    });
    it('setRenderType() deve retornar error', () => {
      expect(() => currency.setRenderType('choice')).to.throw(/is not a function/);
    });
  });
});
