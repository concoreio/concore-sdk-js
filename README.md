# Concore SDK

SDK de desenvolvimento da Concore.

É composto de Datacore, Sighcore e Flowcore.

## Desenvolvendo

Depois de clonar esse repositório, é necessário apenas instalar as dependências:

```
$ npm install
```

Pronto, agora você pode desevolver.

## Testes

O SDK é testado tanto em ambiente Node quanto Web, depois de instalar as dependências basta utilizar um dos comandos a seguir para executar os tests:

Para executar testes em ambiente NodeJS
```
$ npm run test
```

Para executar teste em ambiente Web
```
$ npm run test-browser
```

Para executar em somente uma suite de testes
```
$ npm run test-only test/unit/Concore.js
```

## Deploy

Para fazer o deploy basta executar o comando:

```
$ npm publish
```

## Gerando e Publicando Documentação de API

O SDK utiliza do [ESDoc](https://esdoc.org/) para gerar documentação de API.

Para gerar a documentação, basta executar:

```
$ npm run doc
```

Para publicar a documentação de API é necessário o **[awscli](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/installing.html)** instalado previmente.

```
$ aws s3 sync doc/ s3://apiref.concore --acl public-read
```

E a API será publicada.